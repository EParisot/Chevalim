﻿namespace Chevalim
{
    public class Apport
    {
        public string nom { get; set; }
        public string etat { get; set; }
        public string temp { get; set; }
        public string comm { get; set; }

        public int lbcategorie { get; set; }
        public int lbpoids { get; set; }
        public int lbniveau { get; set; }
        public int lbetat { get; set; }

        public int cbindex { get; set; }
        public decimal Q { get; set; }
    }
}
