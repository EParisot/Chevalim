﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Chevalim
{
    [Serializable()]
    public class ListApports : List<Apport>     //On hérite d'une liste de personnes.
    {

        /// <summary>
        /// Enregistre l'état courant de la classe dans un fichier au format XML.
        /// </summary>
        /// <param name="chemin"></param>
        /// 
        public void Enregistrer(string chemin)
        {
            XmlSerializer xs = new XmlSerializer(typeof(ListApports));
            StreamWriter r = new StreamWriter(chemin);
            xs.Serialize(r, this);
            r.Close();
        }

        public static ListApports Charger(string chemin)
        {
            XmlSerializer xs = new XmlSerializer(typeof(ListApports));
            StreamReader r = new StreamReader(chemin);
            ListApports Ration = (ListApports)xs.Deserialize(r);
            r.Close();

            return Ration;
        }
    }
}