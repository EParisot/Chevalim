﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Data;
using System.IO;
using System.Windows.Input;

namespace Chevalim
{

    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        private void Window_Loaded(object sender, RoutedEventArgs e)                                                                        // Charge les tables de la Bdd, met a zero la feuille de calcul
        {
            SplashScreen screen = new SplashScreen(@"Images\splashscreen.bmp");
            screen.Show(true);

            Mouse.OverrideCursor = Cursors.Wait;

            LoadRessources();

            cbaliment1.SelectedIndex = 0;
            textboxQ1.Text = string.Empty;

            cbaliment2.SelectedIndex = 0;
            textboxQ2.Text = string.Empty;
            gridapports.RowDefinitions[2].Height = new GridLength(0);

            cbaliment3.SelectedIndex = 0;
            textboxQ3.Text = string.Empty;
            gridapports.RowDefinitions[3].Height = new GridLength(0);

            cbaliment4.SelectedIndex = 0;
            textboxQ4.Text = string.Empty;
            gridapports.RowDefinitions[4].Height = new GridLength(0);

            cbaliment5.SelectedIndex = 0;
            textboxQ5.Text = string.Empty;
            gridapports.RowDefinitions[5].Height = new GridLength(0);

            cbaliment6.SelectedIndex = 0;
            textboxQ6.Text = string.Empty;
            gridapports.RowDefinitions[6].Height = new GridLength(0);

            cbaliment7.SelectedIndex = 0;
            textboxQ7.Text = string.Empty;
            gridapports.RowDefinitions[7].Height = new GridLength(0);

            cbaliment8.SelectedIndex = 0;
            textboxQ8.Text = string.Empty;
            gridapports.RowDefinitions[8].Height = new GridLength(0);

            Mouse.OverrideCursor = null;

            screen.Show(false);
        }

        private void LoadRessources()                                                                                                       // Refresh les Comboboxes et la Table des Aliments
        {
            Mouse.OverrideCursor = Cursors.Wait;
            using (var context = new BddChevalimEntities())
            {

                var query1 = context.TableAliments.Where(c => c.Id >0);
                var results1 = query1.ToList();

                tableAlimentsDataGrid.ItemsSource = results1;

                var query2 = from c in context.TableAliments
                            where c.Id > 0
                            select new { c.ImagePath, c.Aliments };
                var results2 = query2.ToList();

                
                cbaliment1.ItemsSource = results2;
                cbalim1.ItemsSource = results2;
                
                cbaliment2.ItemsSource = results2;
                cbalim2.ItemsSource = results2;

                cbaliment3.ItemsSource = results2;
                cbalim3.ItemsSource = results2;

                cbaliment4.ItemsSource = results2;
                cbalim4.ItemsSource = results2;

                cbaliment5.ItemsSource = results2;
                cbalim5.ItemsSource = results2;

                cbaliment6.ItemsSource = results2;
                cbalim6.ItemsSource = results2;

                cbaliment7.ItemsSource = results2;
                cbalim7.ItemsSource = results2;

                cbaliment8.ItemsSource = results2;
                cbalim8.ItemsSource = results2;

                cbalimtoedit.ItemsSource = results2;

                if (modified)
                {
                    int cba1 = cbaliment1.SelectedIndex;
                    cbaliment1.SelectedIndex = -1;
                    cbaliment1.SelectedIndex = cba1;

                    int cba2 = cbaliment2.SelectedIndex;
                    cbaliment2.SelectedIndex = -1;
                    cbaliment2.SelectedIndex = cba2;

                    int cba3 = cbaliment3.SelectedIndex;
                    cbaliment3.SelectedIndex = -1;
                    cbaliment3.SelectedIndex = cba3;

                    int cba4 = cbaliment4.SelectedIndex;
                    cbaliment4.SelectedIndex = -1;
                    cbaliment4.SelectedIndex = cba4;

                    int cba5 = cbaliment5.SelectedIndex;
                    cbaliment5.SelectedIndex = -1;
                    cbaliment5.SelectedIndex = cba5;

                    int cba6 = cbaliment6.SelectedIndex;
                    cbaliment6.SelectedIndex = -1;
                    cbaliment6.SelectedIndex = cba6;

                    int cba7 = cbaliment7.SelectedIndex;
                    cbaliment7.SelectedIndex = -1;
                    cbaliment7.SelectedIndex = cba7;

                    int cba8 = cbaliment8.SelectedIndex;
                    cbaliment8.SelectedIndex = -1;
                    cbaliment8.SelectedIndex = cba8;

                    modified = false;

                }

            }

            Mouse.OverrideCursor = null;
        }


        public MainWindow()
        {
            InitializeComponent();

            AppDomain.CurrentDomain.SetData("DataDirectory",
            Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));

        }


        private void SauvegarderBdd_Click(object sender, RoutedEventArgs e)                                                                   // Click Sauvegarder la Bdd
        {
            Microsoft.Win32.SaveFileDialog savedlg = new Microsoft.Win32.SaveFileDialog();
            savedlg.FileName = "MaBddChevalim";
            savedlg.DefaultExt = ".mdf";
            savedlg.Filter = "Mdf documents (.mdf)|*.mdf";

            Nullable<bool> result = savedlg.ShowDialog();

            if (result == true)
            {
                if (File.Exists(savedlg.FileName))
                    File.Delete(savedlg.FileName);

                string directory;
                directory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Chevalim\BddChevalim.mdf";

                using (BddChevalimEntities dbEntities = new BddChevalimEntities())
                {
                    string backupQuery = @"BACKUP DATABASE ""{0}"" TO DISK = N'{1}'";
                    backupQuery = string.Format(backupQuery, directory, savedlg.FileName);
                    dbEntities.Database.SqlQuery<object>(backupQuery).ToList().FirstOrDefault();
                }
            }
        }

        private void ChargerBdd_Click(object sender, RoutedEventArgs e)                                                                       // Click Restaurer la Bdd
        {
            if (MessageBox.Show("Voulez vous vraiment Charger une nouvelle Base de Données ? \r\n\r\n(Ceci effacera la Base de Données actuelle)", "Confirmation Chargement Base de Données", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {

                Microsoft.Win32.OpenFileDialog opendlg = new Microsoft.Win32.OpenFileDialog();
                opendlg.Filter = "Mdf documents (.mdf)|*.mdf";
                opendlg.DefaultExt = ".mdf";

                Nullable<bool> result = opendlg.ShowDialog();

                if (result == true)
                {
                    Mouse.OverrideCursor = Cursors.Wait;
          
                    using (BddChevalimEntities dbEntities = new BddChevalimEntities())
                    {
                        string directory;
                        directory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Chevalim\BddChevalim.mdf";

                        string restoreQuery = @"USE [Master]; 
                                                ALTER DATABASE ""{0}"" SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
                                                RESTORE DATABASE ""{0}"" FROM DISK='{1}' WITH REPLACE;
                                                ALTER DATABASE ""{0}"" SET MULTI_USER;";
                        restoreQuery = string.Format(restoreQuery, directory, opendlg.FileName);
                        var list = dbEntities.Database.SqlQuery<object>(restoreQuery).ToList();
                        var Result = list.FirstOrDefault();

                    }

                    LoadRessources();
                    Mouse.OverrideCursor = null;
                }
            }
        }
    

        private void Sauvegarder_Click(object sender, RoutedEventArgs e)                                                                    // Sauvegarder Ration
        {
            ListApports Ration = new ListApports();

            Apport Apport0 = new Apport();

            Apport0.nom = tbNom.Text;
            Apport0.etat = tbEtat.Text;
            Apport0.temp = tbTemp.Text;
            Apport0.comm = tbComm.Text;

            Apport0.lbcategorie = listBoxCategorie.SelectedIndex;
            Apport0.lbpoids = listBoxPoids.SelectedIndex;
            Apport0.lbniveau = listBoxNiveau.SelectedIndex;
            Apport0.lbetat = listBoxEtat.SelectedIndex;

            Ration.Add(Apport0);

            if (Apport1.cbindex > 0)
                Ration.Add(Apport1);

            if (Apport2.cbindex > 0)
                Ration.Add(Apport2);

            if (Apport3.cbindex > 0)
                Ration.Add(Apport3);

            if (Apport4.cbindex > 0)
                Ration.Add(Apport4);

            if (Apport5.cbindex > 0)
                Ration.Add(Apport5);

            if (Apport6.cbindex > 0)
                Ration.Add(Apport6);

            if (Apport7.cbindex > 0)
                Ration.Add(Apport7);

            if (Apport8.cbindex > 0)
                Ration.Add(Apport8);

            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Ration "+tbNom.Text; 
            dlg.DefaultExt = ".xml"; 
            dlg.Filter = "Xml documents (.xml)|*.xml"; 

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                Ration.Enregistrer(dlg.FileName);

                MessageBox.Show("Ration enregistrée avec succès. \r\n\r\nSi vous utilisez des aliments personnalisés, pensez à également enregistrer votre Base de Données.", "Enregistrer ma ration");
            }
        }

        private void Charger_Click(object sender, RoutedEventArgs e)                                                                        // Charger Ration
        {
            if (MessageBox.Show("Voulez vous vraiment Charger une nouvelle ration ? \r\n\r\n(Ceci effacera la ration actuelle) \r\n\r\nSi vous utilisez des aliments personnalisés, pensez à importer la Base de Données adaptée.", "Confirmation Chargement Ration", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {

                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                dlg.FileName = "MaRation";
                dlg.DefaultExt = ".xml"; 
                dlg.Filter = "Xml documents (.xml)|*.xml"; 

                Nullable<bool> result = dlg.ShowDialog();

                if (result == true)
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                    rowCount.Clear();
                    rowCount.Add(1);

                    maingrid.RowDefinitions[1].Height = new GridLength(1.5, GridUnitType.Star);

                    gridapports.RowDefinitions[2].Height = new GridLength(0);
                    gridapports.RowDefinitions[3].Height = new GridLength(0);
                    gridapports.RowDefinitions[4].Height = new GridLength(0);
                    gridapports.RowDefinitions[5].Height = new GridLength(0);
                    gridapports.RowDefinitions[6].Height = new GridLength(0);
                    gridapports.RowDefinitions[7].Height = new GridLength(0);
                    gridapports.RowDefinitions[8].Height = new GridLength(0);


                    ListApports Ration = ListApports.Charger(dlg.FileName);

                     tbNom.Text = Ration[0].nom;
                     tbEtat.Text = Ration[0].etat;
                     tbTemp.Text = Ration[0].temp;
                     tbComm.Text = Ration[0].comm;

                    listBoxCategorie.SelectedIndex = Ration[0].lbcategorie;
                    listBoxPoids.SelectedIndex = Ration[0].lbpoids;
                    listBoxNiveau.SelectedIndex = Ration[0].lbniveau;
                    listBoxEtat.SelectedIndex = Ration[0].lbetat;

                    if (Ration.Count > 1)
                    {

                        cbaliment1.SelectedIndex = (int)Ration[1].cbindex;
                        textboxQ1.Text = Ration[1].Q.ToString();
                    }

                    if (Ration.Count > 2)
                    {
                        gridapports.RowDefinitions[2].Height = new GridLength(1, GridUnitType.Star);
                        rowCount.Add(2);

                        cbaliment2.SelectedIndex = (int)Ration[2].cbindex;
                        textboxQ2.Text = Ration[2].Q.ToString();
                    }

                    if (Ration.Count > 3)
                    {
                        gridapports.RowDefinitions[3].Height = new GridLength(1, GridUnitType.Star);
                        rowCount.Add(3);

                        cbaliment3.SelectedIndex = (int)Ration[3].cbindex;
                        textboxQ3.Text = Ration[3].Q.ToString();
                    }

                    if (Ration.Count > 4)
                    {
                        gridapports.RowDefinitions[4].Height = new GridLength(1, GridUnitType.Star);
                        rowCount.Add(4);

                        cbaliment4.SelectedIndex = (int)Ration[4].cbindex;
                        textboxQ4.Text = Ration[4].Q.ToString();
                    }

                    if (Ration.Count > 5)
                    {
                        gridapports.RowDefinitions[5].Height = new GridLength(1, GridUnitType.Star);
                        rowCount.Add(5);

                        cbaliment5.SelectedIndex = (int)Ration[5].cbindex;
                        textboxQ5.Text = Ration[5].Q.ToString();
                    }

                    if (Ration.Count > 6)
                    {
                        gridapports.RowDefinitions[6].Height = new GridLength(1, GridUnitType.Star);
                        rowCount.Add(6);

                        cbaliment6.SelectedIndex = (int)Ration[6].cbindex;
                        textboxQ6.Text = Ration[6].Q.ToString();
                    }

                    if (Ration.Count > 7)
                    {
                        gridapports.RowDefinitions[7].Height = new GridLength(1, GridUnitType.Star);
                        rowCount.Add(7);

                        cbaliment7.SelectedIndex = (int)Ration[7].cbindex;
                        textboxQ7.Text = Ration[7].Q.ToString();
                    }

                    if (Ration.Count > 8)
                    {
                        gridapports.RowDefinitions[8].Height = new GridLength(1, GridUnitType.Star);
                        rowCount.Add(8);

                        cbaliment8.SelectedIndex = (int)Ration[8].cbindex;
                        textboxQ8.Text = Ration[8].Q.ToString();
                    }

                    Mouse.OverrideCursor = null;


                }
            }
        }


        private void Imprimer_Click(object sender, RoutedEventArgs e)                                                                       // Declenche l'Impression
        {
            PrintDialog printDialog = new PrintDialog();

            bool? dialogResult = printDialog.ShowDialog();
            if (dialogResult.HasValue && dialogResult.Value)
            {
                if (TabControl.SelectedIndex == 0)
                    printDialog.PrintVisual(RationSP, "Chevalim : Ration " + tbNom);

                if (TabControl.SelectedIndex == 1)
                    printDialog.PrintVisual(PrixSP, "Chevalim : Prix de la Ration " + tbNom);

                if (TabControl.SelectedIndex == 2)
                    printDialog.PrintVisual(PoidsSP, "Chevalim : Estimateur de Poids " + tbNom);

                if (TabControl.SelectedIndex == 3)
                    printDialog.PrintVisual(TableAlimSP, "Chevalim : Aliments");

            }
        }


        private void MenuLexique_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult lexique = MessageBox.Show("- MS (en Kg ou %) = Matières Sèches \r\n\r\n- UFC = Unités Fourragères Cheval \r\n(1 UFC = 2250 Kcal soit la valeur énergétique nette d’un kg brut d’orge de référence) \r\n\r\n- MADC (en g) = Matières Azotées Digestibles Cheval ", "Lexique/abréviations des termes utilisés dans Chevalim : ");
        }

        private void MenuAide_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult aide = MessageBox.Show("Bienvenue sur Chevalim \r\n\r\n\r\nChevalim est un calculateur dynamique de rations alimentaires pour chevaux. \r\n\r\nCe logiciel va vous donner la possibilité de caluler, évaluer, modifier à volonté une ration alimentaire pour votre cheval / jument / poulain / entier. \r\n\r\n\r\n- Pour commencer, veuillez selectionner une \"Catégorie\", puis un \"Poids\" (si vous ne connaissez pas le poids de votre cheval, un estimateur est à votre disposition au 3ème onglet), un \"Niveau\", \"Age\", \"Stade\" ou \"Service\", et eventuellement une durée de \"Gestation\" ou \"Allaitement\". \r\nLes besoins spécifiques de votre cheval apparaissent alors dans le tableau \"Besoins\". \r\n\r\n- Ensuite vous pouvez séléctionner un aliment à l'aide du menu déroulant (Aliment 1) et lui attribuer une quantité distribuée quotidiennement (en Kg) dans la case correspondante de la même ligne. \r\n\r\n- Vous pourrez également ajouter des aliments à votre ration avec le bouton \"+\" situé sous le premier aliment (jusqu'à 8 aliments différents) ou en supprimer avec le bouton \"-\" situé sur la ligne correspondante. \r\n\r\n- Ajustez la ration selon le code couleur suivant : \r\n\r\nBleu = apport insuffisant \r\nVert = apport correct \r\nRouge = apport en excès \r\n\r\n\r\nVous pouvez modifier le seuil de coloration des cellules de la ligne \"Différence Apports-Bseoins\". \r\n\r\nChaque valeur reste modifiable à tout moment du calcul. \r\n\r\n- Vous pouvez ajouter vos propres aliments à la Table des Aliment dans l'onglet correspondant, et modifier chaque valeur à tout instant. \r\n\r\n- Une fois vos ajustements terminés, enregistrez votre ration depuis le menu principal (\"Mes Rations/Enregistrer ma ration\"). \r\n\r\nATTENTION : Lorsque vous ouvrez une ration depuis le menu principal (\"Mes Rations/Ouvrir une ration\"), veillez à bien disposer de la même Base de Données que lors de son enregistrement afin que tous les aliments de la ration soient disponibles dans la Base de Données. \r\n\r\n- Vous pouvez charger une Base de Données personnalisée ou réinitialiser votre Base de Données depuis le menu principal (\"Base de Données/Importer une base de données\"). \r\nVous pouvez à tout instant re-télécharger et Importer une Base de Données vierge sur www.chevalim.free.fr", "Aide");
        }

        private void MenuInfos_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult infos = MessageBox.Show("Chevalim version : 1.1 \r\nchevalim.free.fr \r\n\r\nConcept / Developpement : Eric PARISOT \r\n\r\nSources : \r\n\r\n- \"Alimentation du cheval\" \r\nRoger Wolter, éditions France Agricole \r\n\r\n- \"Nutrition et alimentation des animaux d'élevage\" Volume 2 \r\nCarole Drogoul, Raymond Gadoud, Marie-Madeleine Joseph, Roland Jussiau \r\n\r\n- \"Alimentation des chevaux : Tables des apports alimentaires INRA 2011\" \r\nLes Haras Nationaux, Coordinateur : William Martin-Rosset ; Editions Quae \r\n\r\n- http://www.ifce.fr/haras-nationaux/", "Infos");
        }



        private void listBoxCategorie_SelectionChanged(object sender, SelectionChangedEventArgs e)                                          // Evenement "selection changée" de la liste Catégorie
        {

            maingrid.RowDefinitions[1].Height = new GridLength(0);

            btninfos.Visibility = Visibility.Hidden;

            textBlockPoids.Text = string.Empty;
            listBoxPoids.Items.Clear();
            listBoxPoids.Opacity = 0;

            textBlockNiveau.Text = string.Empty;
            listBoxNiveau.Items.Clear();
            listBoxNiveau.Opacity = 0;

            textBlockEtat.Text = string.Empty;
            listBoxEtat.Items.Clear();
            listBoxEtat.Opacity = 0;

      
            string[] tblPoids = new string[9] { "200 Kg", "300 Kg", "400 Kg", "450 Kg", "500 Kg", "550 Kg", "600 Kg", "700 Kg", "800 Kg" };

            if (listBoxCategorie.SelectedIndex == -1)
            {
                textBlockPoids.Text = string.Empty;
                listBoxPoids.Items.Clear();
                listBoxPoids.Opacity = 0;
            }
            else
            if (listBoxCategorie.SelectedIndex == 1)
            {
                textBlockPoids.Text = "Poids Adulte estimé :";
                listBoxPoids.Opacity = 100;
                foreach (string poids in tblPoids)
                    listBoxPoids.Items.Add(poids);
            }
            else
            {
                textBlockPoids.Text = "Poids :";
                listBoxPoids.Opacity = 100;
                foreach (string poids in tblPoids)
                    listBoxPoids.Items.Add(poids);
            }


        }

        private void listBoxPoids_SelectionChanged(object sender, SelectionChangedEventArgs e)                                              // Evenement "selection changée" de la liste Poids
        {
            maingrid.RowDefinitions[1].Height = new GridLength(0);

            textBlockNiveau.Text = string.Empty;
            listBoxNiveau.Items.Clear();
            listBoxNiveau.Opacity = 0;

            textBlockEtat.Text = string.Empty;
            listBoxEtat.Items.Clear();
            listBoxEtat.Opacity = 0;

            string[] tblTravail = new string[7] { "Repos", "Repos temporaire", "Très léger", "Léger", "Moyen", "Intense", "Très intense" };
            string[] tblTravail1 = new string[6] { "Repos", "Repos temporaire", "Très léger", "Léger", "Moyen", "Intense" };
            string[] tblTravail2 = new string[5] { "Repos", "Repos temporaire", "Léger", "Moyen", "Intense" };
            string[] tblAge = new string[3] { "6 - 12 mois", "18 - 24 mois", "30 - 36 mois" };
            string[] tblEtat = new string[3] { "Tarie / début gestation", "Gestation", "Allaitement" };
            string[] tblService = new string[4] { "Hors monte", "Léger", "Moyen", "Intense" };

            switch (listBoxCategorie.SelectedIndex)
            {
                case 0:
                    switch (listBoxPoids.SelectedIndex)
                    {
                        case -1:
                            btninfos.Visibility = Visibility.Hidden;
                            break;
                        case 0:
                            textBlockNiveau.Text = "Niveau de Travail :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Visible;

                            foreach (string niveau in tblTravail1)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 1:

                            textBlockNiveau.Text = "Niveau de Travail :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Visible;

                            foreach (string niveau in tblTravail)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 2:

                            textBlockNiveau.Text = "Niveau de Travail :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Visible;

                            foreach (string niveau in tblTravail)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 3:

                            textBlockNiveau.Text = "Niveau de Travail :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Visible;

                            foreach (string niveau in tblTravail)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 4:
                            textBlockNiveau.Text = "Niveau de Travail :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Visible;

                            foreach (string niveau in tblTravail)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 5:

                            textBlockNiveau.Text = "Niveau de Travail :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Visible;

                            foreach (string niveau in tblTravail)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 6:

                            textBlockNiveau.Text = "Niveau de Travail :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Visible;

                            foreach (string niveau in tblTravail)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 7:

                            textBlockNiveau.Text = "Niveau de Travail :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Visible;

                            foreach (string niveau in tblTravail2)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 8:

                            textBlockNiveau.Text = "Niveau de Travail :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Visible;

                            foreach (string niveau in tblTravail2)
                                listBoxNiveau.Items.Add(niveau);
                            break;


                    }

                    break;
                case 1:
                    switch (listBoxPoids.SelectedIndex)
                    {
                        case 0:
                            textBlockNiveau.Text = "Age :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Hidden;

                            foreach (string niveau in tblAge)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 1:
                            textBlockNiveau.Text = "Age :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Hidden;

                            foreach (string niveau in tblAge)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 2:
                            textBlockNiveau.Text = "Age :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Hidden;

                            foreach (string niveau in tblAge)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 3:
                            textBlockNiveau.Text = "Age :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Hidden;

                            foreach (string niveau in tblAge)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 4:
                            textBlockNiveau.Text = "Age :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Hidden;

                            foreach (string niveau in tblAge)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 5:
                            textBlockNiveau.Text = "Age :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Hidden;

                            foreach (string niveau in tblAge)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 6:
                            textBlockNiveau.Text = "Age :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Hidden;

                            foreach (string niveau in tblAge)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 7:
                            textBlockNiveau.Text = "Age :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Hidden;

                            foreach (string niveau in tblAge)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 8:
                            textBlockNiveau.Text = "Age :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Hidden;

                            foreach (string niveau in tblAge)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                   

                    }

                    break;
                case 2:
                    switch (listBoxPoids.SelectedIndex)
                    {
                        case 0:
                            textBlockNiveau.Text = "Stade :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Hidden;

                            foreach (string niveau in tblEtat)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 1:
                            textBlockNiveau.Text = "Stade :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Hidden;

                            foreach (string niveau in tblEtat)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 2:
                            textBlockNiveau.Text = "Stade :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Hidden;

                            foreach (string niveau in tblEtat)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 3:
                            textBlockNiveau.Text = "Stade :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Hidden;

                            foreach (string niveau in tblEtat)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 4:
                            textBlockNiveau.Text = "Stade :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Hidden;

                            foreach (string niveau in tblEtat)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 5:
                            textBlockNiveau.Text = "Stade :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Hidden;

                            foreach (string niveau in tblEtat)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 6:
                            textBlockNiveau.Text = "Stade :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Hidden;

                            foreach (string niveau in tblEtat)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 7:
                            textBlockNiveau.Text = "Stade :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Hidden;

                            foreach (string niveau in tblEtat)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 8:
                            textBlockNiveau.Text = "Stade :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Hidden;

                            foreach (string niveau in tblEtat)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                    }

                    break;
                case 3:
                    switch (listBoxPoids.SelectedIndex)
                    {
                        case 0:
                            textBlockNiveau.Text = "Service :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Visible;

                            foreach (string niveau in tblService)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 1:
                            textBlockNiveau.Text = "Service :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Visible;

                            foreach (string niveau in tblService)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 2:
                            textBlockNiveau.Text = "Service :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Visible;

                            foreach (string niveau in tblService)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 3:
                            textBlockNiveau.Text = "Service :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Visible;

                            foreach (string niveau in tblService)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 4:
                            textBlockNiveau.Text = "Service :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Visible;

                            foreach (string niveau in tblService)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 5:
                            textBlockNiveau.Text = "Service :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Visible;

                            foreach (string niveau in tblService)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 6:
                            textBlockNiveau.Text = "Service :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Visible;

                            foreach (string niveau in tblService)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 7:
                            textBlockNiveau.Text = "Service :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Visible;

                            foreach (string niveau in tblService)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                        case 8:
                            textBlockNiveau.Text = "Service :";
                            listBoxNiveau.Opacity = 100;
                            btninfos.Visibility = Visibility.Visible;

                            foreach (string niveau in tblService)
                                listBoxNiveau.Items.Add(niveau);
                            break;
                    }
                    break;
            }


        }

        private void listBoxNiveau_SelectionChanged(object sender, SelectionChangedEventArgs e)                                             // Evenement "selection changée" de la liste Niveau
        {
            Mouse.OverrideCursor = Cursors.Wait;

            textBlockEtat.Text = string.Empty;
            listBoxEtat.Items.Clear();
            listBoxEtat.Opacity = 0;

            string[] tblGest = new string[7] { "0 - 5 mois", "6ème mois", "7ème mois", "8ème mois", "9ème mois", "10ème mois", "11ème mois" };
            string[] tblAllait = new string[6] { "1er mois", "2ème mois", "3ème mois", "4ème mois", "5ème mois", "6ème mois" };

            switch (listBoxNiveau.SelectedIndex)
            {

                default:
                    maingrid.RowDefinitions[1].Height = new GridLength(0);
                    textBlockEtat.Text = string.Empty;
                    listBoxEtat.Items.Clear();
                    listBoxEtat.Opacity = 0;

                    IdBesoins();
                    DisplayBesoin(idbesoin);
                    Somme();
                    Difference();

                    break;

                case 0:
                    maingrid.RowDefinitions[1].Height = new GridLength(1.5, GridUnitType.Star);
                    listBoxEtat.Opacity = 0;

                    IdBesoins();
                    DisplayBesoin(idbesoin);
                    Somme();
                    Difference();

                    break;

                case 1:

                    if (listBoxCategorie.SelectedIndex == 2)
                    {
                        maingrid.RowDefinitions[1].Height = new GridLength(0);

                        textBlockEtat.Text = "Gestation :";
                        listBoxEtat.Opacity = 100;

                        foreach (string etat in tblGest)
                            listBoxEtat.Items.Add(etat);
                    }
                    else
                    {
                        maingrid.RowDefinitions[1].Height = new GridLength(1.5, GridUnitType.Star);
                        listBoxEtat.Opacity = 0;
                        IdBesoins();
                        DisplayBesoin(idbesoin);
                        Somme();
                        Difference();
                    }
                    break;

                case 2:

                    if (listBoxCategorie.SelectedIndex == 2)
                    {
                        maingrid.RowDefinitions[1].Height = new GridLength(0);

                        textBlockEtat.Text = "Allaitement :";
                        listBoxEtat.Opacity = 100;

                        foreach (string etat in tblAllait)
                            listBoxEtat.Items.Add(etat);
                    }
                    else
                    {
                        maingrid.RowDefinitions[1].Height = new GridLength(1.5, GridUnitType.Star);
                        listBoxEtat.Opacity = 0;
                        IdBesoins();
                        DisplayBesoin(idbesoin);
                        Somme();
                        Difference();
                    }
                    break;

                case 3:
                    maingrid.RowDefinitions[1].Height = new GridLength(1.5, GridUnitType.Star);
                    listBoxEtat.Opacity = 0;

                    IdBesoins();
                    DisplayBesoin(idbesoin);
                    Somme();
                    Difference();

                    break;

                case 4:
                    maingrid.RowDefinitions[1].Height = new GridLength(1.5, GridUnitType.Star);
                    listBoxEtat.Opacity = 0;

                    IdBesoins();
                    DisplayBesoin(idbesoin);
                    Somme();
                    Difference();

                    break;

                case 5:
                    maingrid.RowDefinitions[1].Height = new GridLength(1.5, GridUnitType.Star);
                    listBoxEtat.Opacity = 0;

                    IdBesoins();
                    DisplayBesoin(idbesoin);
                    Somme();
                    Difference();

                    break;

                case 6:
                    maingrid.RowDefinitions[1].Height = new GridLength(1.5, GridUnitType.Star);
                    listBoxEtat.Opacity = 0;

                    IdBesoins();
                    DisplayBesoin(idbesoin);
                    Somme();
                    Difference();

                    break;

            }
            Mouse.OverrideCursor = null;
        }

        private void listBoxEtat_SelectionChanged(object sender, SelectionChangedEventArgs e)                                               // Evenement "selection changée" de la liste Etat
        {
            Mouse.OverrideCursor = Cursors.Wait;
            switch (listBoxEtat.SelectedIndex)
            {
                default:
                    maingrid.RowDefinitions[1].Height = new GridLength(0);
                    break;
                case 0:
                    maingrid.RowDefinitions[1].Height = new GridLength(1.5, GridUnitType.Star);
                    break;
                case 1:
                    maingrid.RowDefinitions[1].Height = new GridLength(1.5, GridUnitType.Star);
                    break;
                case 2:
                    maingrid.RowDefinitions[1].Height = new GridLength(1.5, GridUnitType.Star);
                    break;
                case 3:
                    maingrid.RowDefinitions[1].Height = new GridLength(1.5, GridUnitType.Star);
                    break;
                case 4:
                    maingrid.RowDefinitions[1].Height = new GridLength(1.5, GridUnitType.Star);
                    break;
                case 5:
                    maingrid.RowDefinitions[1].Height = new GridLength(1.5, GridUnitType.Star);
                    break;
                case 6:
                    maingrid.RowDefinitions[1].Height = new GridLength(1.5, GridUnitType.Star);
                    break;
            }

            IdBesoins();
            DisplayBesoin(idbesoin);
            Somme();
            Difference();

            Mouse.OverrideCursor = null;
        }


        private void btninfos_Click(object sender, RoutedEventArgs e)
        {
            if (listBoxCategorie.SelectedIndex == 0)
            {
                MessageBoxResult infoniveautravail = MessageBox.Show("- Très léger = 1 à 2 h par jour de travail léger \r\n\r\n- Léger = 2 à 3 h par jour de travail léger \r\n\r\n- Moyen = 1 à 2 h par jour de travail modéré \r\n\r\n- Intense = 1 h par jour de travail intense (ex : préparation compétitions) \r\n\r\n-Très intense = 1 à 2 h par jour de travail intense (ex : compétition)", "Niveaux de Travail : ");
            }
            if (listBoxCategorie.SelectedIndex == 3)
            {
                MessageBoxResult infoniveauservice = MessageBox.Show("- Léger = 1 saillie tous les deux jours \r\n\r\n- Moyen = 1 saillie par jour \r\n\r\n- Intense = 2 saillies par jour", "Niveaux de Service : ");
            }
        }



        int idbesoin;   
                                                                                                                                            // Variable correspondant au besoin selectionné
        private int IdBesoins()                                                                                                             // Arborescence de besoins => identifiant idbesoin
        {

            switch (listBoxCategorie.SelectedIndex)
            {
                default:
                    idbesoin = 0;
                    break;

                case 0:

                    switch (listBoxPoids.SelectedIndex)
                    {

                        default:
                            idbesoin = 0;
                            break;

                        case 0:
                            switch (listBoxNiveau.SelectedIndex)
                            {

                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 1;
                                    break;
                                case 1:
                                    idbesoin = 2;
                                    break;
                                case 2:
                                    idbesoin = 3;
                                    break;
                                case 3:
                                    idbesoin = 4;
                                    break;
                                case 4:
                                    idbesoin = 5;
                                    break;
                                case 5:
                                    idbesoin = 6;
                                    break;
                            }

                            break;

                        case 1:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 7;
                                    break;
                                case 1:
                                    idbesoin = 8;
                                    break;
                                case 2:
                                    idbesoin = 9;
                                    break;
                                case 3:
                                    idbesoin = 10;
                                    break;
                                case 4:
                                    idbesoin = 11;
                                    break;
                                case 5:
                                    idbesoin = 12;
                                    break;
                                case 6:
                                    idbesoin = 13;
                                    break;

                            }
                            break;

                        case 2:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 14;
                                    break;
                                case 1:
                                    idbesoin = 15;
                                    break;
                                case 2:
                                    idbesoin = 16;
                                    break;
                                case 3:
                                    idbesoin = 17;
                                    break;
                                case 4:
                                    idbesoin = 18;
                                    break;
                                case 5:
                                    idbesoin = 19;
                                    break;
                                case 6:
                                    idbesoin = 20;
                                    break;
                            }
                            break;

                        case 3:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 21;
                                    break;
                                case 1:
                                    idbesoin = 22;
                                    break;
                                case 2:
                                    idbesoin = 23;
                                    break;
                                case 3:
                                    idbesoin = 24;
                                    break;
                                case 4:
                                    idbesoin = 25;
                                    break;
                                case 5:
                                    idbesoin = 26;
                                    break;
                                case 6:
                                    idbesoin = 27;
                                    break;
                            }
                            break;
                        case 4:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 28;
                                    break;
                                case 1:
                                    idbesoin = 29;
                                    break;
                                case 2:
                                    idbesoin = 30;
                                    break;
                                case 3:
                                    idbesoin = 31;
                                    break;
                                case 4:
                                    idbesoin = 32;
                                    break;
                                case 5:
                                    idbesoin = 33;
                                    break;
                                case 6:
                                    idbesoin = 34;
                                    break;
                            }
                            break;
                        case 5:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 35;
                                    break;
                                case 1:
                                    idbesoin = 36;
                                    break;
                                case 2:
                                    idbesoin = 37;
                                    break;
                                case 3:
                                    idbesoin = 38;
                                    break;
                                case 4:
                                    idbesoin = 39;
                                    break;
                                case 5:
                                    idbesoin = 40;
                                    break;
                                case 6:
                                    idbesoin = 41;
                                    break;
                            }
                            break;
                        case 6:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 42;
                                    break;
                                case 1:
                                    idbesoin = 43;
                                    break;
                                case 2:
                                    idbesoin = 44;
                                    break;
                                case 3:
                                    idbesoin = 45;
                                    break;
                                case 4:
                                    idbesoin = 46;
                                    break;
                                case 5:
                                    idbesoin = 47;
                                    break;
                                case 6:
                                    idbesoin = 48;
                                    break;
                            }
                            break;
                        case 7:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 49;
                                    break;
                                case 1:
                                    idbesoin = 50;
                                    break;
                                case 2:
                                    idbesoin = 51;
                                    break;
                                case 3:
                                    idbesoin = 52;
                                    break;
                                case 4:
                                    idbesoin = 53;
                                    break;
                                    //saut
                            }
                            break;
                        case 8:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 54;
                                    break;
                                case 1:
                                    idbesoin = 55;
                                    break;
                                case 2:
                                    idbesoin = 56;
                                    break;
                                case 3:
                                    idbesoin = 57;
                                    break;
                                case 4:
                                    idbesoin = 58;
                                    break;
                                    //saut

                            }
                            break;
                    }

                    break;

                case 1:
                    switch (listBoxPoids.SelectedIndex)
                    {
                        default:
                            idbesoin = 0;
                            break;

                        case 0:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;

                                case 0:
                                    idbesoin = 61;
                                    break;
                                case 1:
                                    idbesoin = 62;
                                    break;
                                case 2:
                                    idbesoin = 63;
                                    break;
                            }

                            break;

                        case 1:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 64;
                                    break;
                                case 1:
                                    idbesoin = 65;
                                    break;
                                case 2:
                                    idbesoin = 66;
                                    break;
                            }
                            break;

                        case 2:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 67;
                                    break;
                                case 1:
                                    idbesoin = 68;
                                    break;
                                case 2:
                                    idbesoin = 69;
                                    break;
                            }
                            break;
                        case 3:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;

                                case 0:
                                    idbesoin = 70;
                                    break;
                                case 1:
                                    idbesoin = 71;
                                    break;
                                case 2:
                                    idbesoin = 72;
                                    break;
                            }
                        
                            break;

                        case 4:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 73;
                                    break;
                                case 1:
                                    idbesoin = 74;
                                    break;
                                case 2:
                                    idbesoin = 75;
                                    break;
                            }
                            break;

                        case 5:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 76;
                                    break;
                                case 1:
                                    idbesoin = 77;
                                    break;
                                case 2:
                                    idbesoin = 78;
                                    break;
                            }
                            break;

                        case 6:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;

                                case 0:
                                    idbesoin = 79;
                                    break;
                                case 1:
                                    idbesoin = 80;
                                    break;
                                case 2:
                                    idbesoin = 81;
                                    break;
                            }

                            break;
                        case 7:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 82;
                                    break;
                                case 1:
                                    idbesoin = 83;
                                    break;
                                case 2:
                                    idbesoin = 84;
                                    break;
                            }
                            break;

                        case 8:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 85;
                                    break;
                                case 1:
                                    idbesoin = 86;
                                    break;
                                case 2:
                                    idbesoin = 87;
                                    break;
                            }
                            break;
                    }
                    break;

                case 2:
                    switch (listBoxPoids.SelectedIndex)
                    {
                        default:
                            idbesoin = 0;
                            break;

                        case 0:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 88;
                                    break;

                                case 1:
                                    switch (listBoxEtat.SelectedIndex)
                                    {
                                        default:
                                            idbesoin = 0;
                                            break;
                                        case 0:
                                            idbesoin = 89;
                                            break;
                                        case 1:
                                            idbesoin = 90;
                                            break;
                                        case 2:
                                            idbesoin = 91;
                                            break;
                                        case 3:
                                            idbesoin = 92;
                                            break;
                                        case 4:
                                            idbesoin = 93;
                                            break;
                                        case 5:
                                            idbesoin = 94;
                                            break;
                                        case 6:
                                            idbesoin = 95;
                                            break;
                                    }
                                    break;
                                case 2:
                                    switch (listBoxEtat.SelectedIndex)
                                    {
                                        default:
                                            idbesoin = 0;
                                            break;
                                        case 0:
                                            idbesoin = 96;
                                            break;
                                        case 1:
                                            idbesoin = 97;
                                            break;
                                        case 2:
                                            idbesoin = 98;
                                            break;
                                        case 3:
                                            idbesoin = 99;
                                            break;
                                        case 4:
                                            idbesoin = 100;
                                            break;
                                        case 5:
                                            idbesoin = 101;
                                            break;
                                    }
                                    break;
                            }

                            break;

                        case 1:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;

                                case 0:
                                    idbesoin = 102;
                                    break;

                                case 1:
                                    switch (listBoxEtat.SelectedIndex)
                                    {
                                        default:
                                            idbesoin = 0;
                                            break;
                                        case 0:
                                            idbesoin = 103;
                                            break;
                                        case 1:
                                            idbesoin = 104;
                                            break;
                                        case 2:
                                            idbesoin = 105;
                                            break;
                                        case 3:
                                            idbesoin = 106;
                                            break;
                                        case 4:
                                            idbesoin = 107;
                                            break;
                                        case 5:
                                            idbesoin = 108;
                                            break;
                                        case 6:
                                            idbesoin = 109;
                                            break;
                                    }
                                    break;

                                case 2:
                                    switch (listBoxEtat.SelectedIndex)
                                    {
                                        default:
                                            idbesoin = 0;
                                            break;
                                        case 0:
                                            idbesoin = 110;
                                            break;
                                        case 1:
                                            idbesoin = 111;
                                            break;
                                        case 2:
                                            idbesoin = 112;
                                            break;
                                        case 3:
                                            idbesoin = 113;
                                            break;
                                        case 4:
                                            idbesoin = 114;
                                            break;
                                        case 5:
                                            idbesoin = 115;
                                            break;
                                    }
                                    break;
                            }
                            break;

                        case 2:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 116;
                                    break;

                                case 1:
                                    switch (listBoxEtat.SelectedIndex)
                                    {
                                        default:
                                            idbesoin = 0;
                                            break;
                                        case 0:
                                            idbesoin = 117;
                                            break;
                                        case 1:
                                            idbesoin = 118;
                                            break;
                                        case 2:
                                            idbesoin = 119;
                                            break;
                                        case 3:
                                            idbesoin = 120;
                                            break;
                                        case 4:
                                            idbesoin = 121;
                                            break;
                                        case 5:
                                            idbesoin = 122;
                                            break;
                                        case 6:
                                            idbesoin = 123;
                                            break;
                                    }
                                    break;

                                case 2:
                                    switch (listBoxEtat.SelectedIndex)
                                    {
                                        default:
                                            idbesoin = 0;
                                            break;
                                        case 0:
                                            idbesoin = 124;
                                            break;
                                        case 1:
                                            idbesoin = 125;
                                            break;
                                        case 2:
                                            idbesoin = 126;
                                            break;
                                        case 3:
                                            idbesoin = 127;
                                            break;
                                        case 4:
                                            idbesoin = 128;
                                            break;
                                        case 5:
                                            idbesoin = 129;
                                            break;
                                    }
                                    break;
                            }
                            break;
                        case 3:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 130;
                                    break;

                                case 1:
                                    switch (listBoxEtat.SelectedIndex)
                                    {
                                        default:
                                            idbesoin = 0;
                                            break;
                                        case 0:
                                            idbesoin = 131;
                                            break;
                                        case 1:
                                            idbesoin = 132;
                                            break;
                                        case 2:
                                            idbesoin = 133;
                                            break;
                                        case 3:
                                            idbesoin = 134;
                                            break;
                                        case 4:
                                            idbesoin = 135;
                                            break;
                                        case 5:
                                            idbesoin = 136;
                                            break;
                                        case 6:
                                            idbesoin = 137;
                                            break;
                                    }
                                    break;

                                case 2:
                                    switch (listBoxEtat.SelectedIndex)
                                    {
                                        default:
                                            idbesoin = 0;
                                            break;
                                        case 0:
                                            idbesoin = 138;
                                            break;
                                        case 1:
                                            idbesoin = 139;
                                            break;
                                        case 2:
                                            idbesoin = 140;
                                            break;
                                        case 3:
                                            idbesoin = 141;
                                            break;
                                        case 4:
                                            idbesoin = 142;
                                            break;
                                        case 5:
                                            idbesoin = 143;
                                            break;
                                    }
                                    break;
                            }
                            break;
                        case 4:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 144;
                                    break;

                                case 1:
                                    switch (listBoxEtat.SelectedIndex)
                                    {
                                        default:
                                            idbesoin = 0;
                                            break;
                                        case 0:
                                            idbesoin = 145;
                                            break;
                                        case 1:
                                            idbesoin = 146;
                                            break;
                                        case 2:
                                            idbesoin = 147;
                                            break;
                                        case 3:
                                            idbesoin = 148;
                                            break;
                                        case 4:
                                            idbesoin = 149;
                                            break;
                                        case 5:
                                            idbesoin = 150;
                                            break;
                                        case 6:
                                            idbesoin = 151;
                                            break;
                                    }
                                    break;

                                case 2:
                                    switch (listBoxEtat.SelectedIndex)
                                    {
                                        default:
                                            idbesoin = 0;
                                            break;
                                        case 0:
                                            idbesoin = 152;
                                            break;
                                        case 1:
                                            idbesoin = 153;
                                            break;
                                        case 2:
                                            idbesoin = 154;
                                            break;
                                        case 3:
                                            idbesoin = 155;
                                            break;
                                        case 4:
                                            idbesoin = 156;
                                            break;
                                        case 5:
                                            idbesoin = 157;
                                            break;
                                    }
                                    break;
                            }
                            break;
                        case 5:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 158;
                                    break;

                                case 1:
                                    switch (listBoxEtat.SelectedIndex)
                                    {
                                        default:
                                            idbesoin = 0;
                                            break;
                                        case 0:
                                            idbesoin = 159;
                                            break;
                                        case 1:
                                            idbesoin = 160;
                                            break;
                                        case 2:
                                            idbesoin = 161;
                                            break;
                                        case 3:
                                            idbesoin = 162;
                                            break;
                                        case 4:
                                            idbesoin = 163;
                                            break;
                                        case 5:
                                            idbesoin = 164;
                                            break;
                                        case 6:
                                            idbesoin = 165;
                                            break;
                                    }
                                    break;

                                case 2:
                                    switch (listBoxEtat.SelectedIndex)
                                    {
                                        default:
                                            idbesoin = 0;
                                            break;
                                        case 0:
                                            idbesoin = 166;
                                            break;
                                        case 1:
                                            idbesoin = 167;
                                            break;
                                        case 2:
                                            idbesoin = 168;
                                            break;
                                        case 3:
                                            idbesoin = 169;
                                            break;
                                        case 4:
                                            idbesoin = 170;
                                            break;
                                        case 5:
                                            idbesoin = 171;
                                            break;
                                    }
                                    break;
                            }
                            break;
                        case 6:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 172;
                                    break;

                                case 1:
                                    switch (listBoxEtat.SelectedIndex)
                                    {
                                        default:
                                            idbesoin = 0;
                                            break;
                                        case 0:
                                            idbesoin = 173;
                                            break;
                                        case 1:
                                            idbesoin = 174;
                                            break;
                                        case 2:
                                            idbesoin = 175;
                                            break;
                                        case 3:
                                            idbesoin = 176;
                                            break;
                                        case 4:
                                            idbesoin = 177;
                                            break;
                                        case 5:
                                            idbesoin = 178;
                                            break;
                                        case 6:
                                            idbesoin = 179;
                                            break;
                                    }
                                    break;

                                case 2:
                                    switch (listBoxEtat.SelectedIndex)
                                    {
                                        default:
                                            idbesoin = 0;
                                            break;
                                        case 0:
                                            idbesoin = 180;
                                            break;
                                        case 1:
                                            idbesoin = 181;
                                            break;
                                        case 2:
                                            idbesoin = 182;
                                            break;
                                        case 3:
                                            idbesoin = 183;
                                            break;
                                        case 4:
                                            idbesoin = 184;
                                            break;
                                        case 5:
                                            idbesoin = 185;
                                            break;
                                    }
                                    break;
                            }
                            break;
                        case 7:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 186;
                                    break;

                                case 1:
                                    switch (listBoxEtat.SelectedIndex)
                                    {
                                        default:
                                            idbesoin = 0;
                                            break;
                                        case 0:
                                            idbesoin = 187;
                                            break;
                                        case 1:
                                            idbesoin = 188;
                                            break;
                                        case 2:
                                            idbesoin = 189;
                                            break;
                                        case 3:
                                            idbesoin = 190;
                                            break;
                                        case 4:
                                            idbesoin = 191;
                                            break;
                                        case 5:
                                            idbesoin = 192;
                                            break;
                                        case 6:
                                            idbesoin = 193;
                                            break;
                                    }
                                    break;

                                case 2:
                                    switch (listBoxEtat.SelectedIndex)
                                    {
                                        default:
                                            idbesoin = 0;
                                            break;
                                        case 0:
                                            idbesoin = 194;
                                            break;
                                        case 1:
                                            idbesoin = 195;
                                            break;
                                        case 2:
                                            idbesoin = 196;
                                            break;
                                        case 3:
                                            idbesoin = 197;
                                            break;
                                        case 4:
                                            idbesoin = 198;
                                            break;
                                        case 5:
                                            idbesoin = 199;
                                            break;
                                    }
                                    break;
                            }
                            break;
                        case 8:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 200;
                                    break;

                                case 1:
                                    switch (listBoxEtat.SelectedIndex)
                                    {
                                        default:
                                            idbesoin = 0;
                                            break;
                                        case 0:
                                            idbesoin = 201;
                                            break;
                                        case 1:
                                            idbesoin = 202;
                                            break;
                                        case 2:
                                            idbesoin = 203;
                                            break;
                                        case 3:
                                            idbesoin = 204;
                                            break;
                                        case 4:
                                            idbesoin = 205;
                                            break;
                                        case 5:
                                            idbesoin = 206;
                                            break;
                                        case 6:
                                            idbesoin = 207;
                                            break;
                                    }
                                    break;

                                case 2:
                                    switch (listBoxEtat.SelectedIndex)
                                    {
                                        default:
                                            idbesoin = 0;
                                            break;
                                        case 0:
                                            idbesoin = 208;
                                            break;
                                        case 1:
                                            idbesoin = 209;
                                            break;
                                        case 2:
                                            idbesoin = 210;
                                            break;
                                        case 3:
                                            idbesoin = 211;
                                            break;
                                        case 4:
                                            idbesoin = 212;
                                            break;
                                        case 5:
                                            idbesoin = 213;
                                            break;
                                    }
                                    break;
                            }
                            break;
                    }
                    break;

                case 3:

                    switch (listBoxPoids.SelectedIndex)
                    {
                        default:
                            idbesoin = 0;
                            break;

                        case 0:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;

                                case 0:
                                    idbesoin = 214;
                                    break;
                                case 1:
                                    idbesoin = 215;
                                    break;
                                case 2:
                                    idbesoin = 216;
                                    break;
                                case 3:
                                    idbesoin = 217;
                                    break;
                            }

                            break;

                        case 1:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 218;
                                    break;
                                case 1:
                                    idbesoin = 219;
                                    break;
                                case 2:
                                    idbesoin = 220;
                                    break;
                                case 3:
                                    idbesoin = 221;
                                    break;
                            }
                            break;

                        case 2:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 222;
                                    break;
                                case 1:
                                    idbesoin = 223;
                                    break;
                                case 2:
                                    idbesoin = 224;
                                    break;
                                case 3:
                                    idbesoin = 225;
                                    break;
                            }
                            break;

                        case 3:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 226;
                                    break;
                                case 1:
                                    idbesoin = 227;
                                    break;
                                case 2:
                                    idbesoin = 228;
                                    break;
                                case 3:
                                    idbesoin = 229;
                                    break;
                            }
                            break;
                        case 4:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;

                                case 0:
                                    idbesoin = 230;
                                    break;
                                case 1:
                                    idbesoin = 231;
                                    break;
                                case 2:
                                    idbesoin = 232;
                                    break;
                                case 3:
                                    idbesoin = 233;
                                    break;
                            }

                            break;

                        case 5:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 234;
                                    break;
                                case 1:
                                    idbesoin = 235;
                                    break;
                                case 2:
                                    idbesoin = 236;
                                    break;
                                case 3:
                                    idbesoin = 237;
                                    break;
                            }
                            break;

                        case 6:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 238;
                                    break;
                                case 1:
                                    idbesoin = 239;
                                    break;
                                case 2:
                                    idbesoin = 240;
                                    break;
                                case 3:
                                    idbesoin = 241;
                                    break;
                            }
                            break;

                        case 7:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 242;
                                    break;
                                case 1:
                                    idbesoin = 243;
                                    break;
                                case 2:
                                    idbesoin = 244;
                                    break;
                                case 3:
                                    idbesoin = 245;
                                    break;
                            }
                            break;
                        case 8:
                            switch (listBoxNiveau.SelectedIndex)
                            {
                                default:
                                    idbesoin = 0;
                                    break;
                                case 0:
                                    idbesoin = 246;
                                    break;
                                case 1:
                                    idbesoin = 247;
                                    break;
                                case 2:
                                    idbesoin = 248;
                                    break;
                                case 3:
                                    idbesoin = 249;
                                    break;
                            }
                            break;
                    }
                    break;


            }
            return idbesoin;
        }

        DonneesP besoin = new DonneesP();

        private void DisplayBesoin(int idbesoin)                                                                                            // Affiche les Besoins en fonction de l'identifiant idbesoin
        {

            using (var context = new BddChevalimEntities())
            {
                if (idbesoin == 0)

                datagridBesoins.ItemsSource = null;

                besoin.MS__Kg_ = 0;
                besoin.UFC = 0;
                besoin.MADC__g_ = 0;
                besoin.Ca__g_ = 0;
                besoin.P__g_ = 0;
                besoin.Zn__mg_ = 0;
                besoin.Cu__mg_ = 0;


                if (idbesoin == 1)
                {
                    var query = from c in context.TableBChx200
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 2)
                {
                    var query = from c in context.TableBChx200
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }
                }

                if (idbesoin == 3)
                {
                    var query = from c in context.TableBChx200
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 4)
                {
                    var query = from c in context.TableBChx200
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 5)
                {
                    var query = from c in context.TableBChx200
                                where c.Id == 5
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 6)
                {
                    var query = from c in context.TableBChx200
                                where c.Id == 6
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 7)
                {
                    var query = from c in context.TableBChx300
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 8)
                {
                    var query = from c in context.TableBChx300
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 9)
                {
                    var query = from c in context.TableBChx300
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }
                }

                if (idbesoin == 10)
                {
                    var query = from c in context.TableBChx300
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }
                }

                if (idbesoin == 11)
                {
                    var query = from c in context.TableBChx300
                                where c.Id == 5
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 12)
                {
                    var query = from c in context.TableBChx300
                                where c.Id == 6
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }
                }

                if (idbesoin == 13)
                {
                    var query = from c in context.TableBChx300
                                where c.Id == 7
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }
                }

                if (idbesoin == 14)
                {
                    var query = from c in context.TableBChx400
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }
                }

                if (idbesoin == 15)
                {
                    var query = from c in context.TableBChx400
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }
                }

                if (idbesoin == 16)
                {
                    var query = from c in context.TableBChx400
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }
                }

                if (idbesoin == 17)
                {
                    var query = from c in context.TableBChx400
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }
                }

                if (idbesoin == 18)
                {
                    var query = from c in context.TableBChx400
                                where c.Id == 5
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }
                }

                if (idbesoin == 19)
                {
                    var query = from c in context.TableBChx400
                                where c.Id == 6
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }
                }

                if (idbesoin == 20)
                {
                    var query = from c in context.TableBChx400
                                where c.Id == 7
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }
                }

                if (idbesoin == 21)
                {
                    var query = from c in context.TableBChx450
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }
                if (idbesoin == 22)
                {
                    var query = from c in context.TableBChx450
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }
                if (idbesoin == 23)
                {
                    var query = from c in context.TableBChx450
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 24)
                {
                    var query = from c in context.TableBChx450
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }
                if (idbesoin == 25)
                {
                    var query = from c in context.TableBChx450
                                where c.Id == 5
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }
                if (idbesoin == 26)
                {
                    var query = from c in context.TableBChx450
                                where c.Id == 6
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }
                }

                if (idbesoin == 27)
                {
                    var query = from c in context.TableBChx450
                                where c.Id == 7
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 28)
                {
                    var query = from c in context.TableBChx500
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 29)
                {
                    var query = from c in context.TableBChx500
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 30)
                {
                    var query = from c in context.TableBChx500
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 31)
                {
                    var query = from c in context.TableBChx500
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 32)
                {
                    var query = from c in context.TableBChx500
                                where c.Id == 5
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 33)
                {
                    var query = from c in context.TableBChx500
                                where c.Id == 6
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 34)
                {
                    var query = from c in context.TableBChx500
                                where c.Id == 7
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 35)
                {
                    var query = from c in context.TableBChx550
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 36)
                {
                    var query = from c in context.TableBChx550
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 37)
                {
                    var query = from c in context.TableBChx550
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 38)
                {
                    var query = from c in context.TableBChx550
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 39)
                {
                    var query = from c in context.TableBChx550
                                where c.Id == 5
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 40)
                {
                    var query = from c in context.TableBChx550
                                where c.Id == 6
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 41)
                {
                    var query = from c in context.TableBChx550
                                where c.Id == 7
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 42)
                {
                    var query = from c in context.TableBChx600
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 43)
                {
                    var query = from c in context.TableBChx600
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 44)
                {
                    var query = from c in context.TableBChx600
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 45)
                {
                    var query = from c in context.TableBChx600
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 46)
                {
                    var query = from c in context.TableBChx600
                                where c.Id == 5
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 47)
                {
                    var query = from c in context.TableBChx600
                                where c.Id == 6
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 48)
                {
                    var query = from c in context.TableBChx600
                                where c.Id == 7
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 49)
                {
                    var query = from c in context.TableBChx700
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 50)
                {
                    var query = from c in context.TableBChx700
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 51)
                {
                    var query = from c in context.TableBChx700
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 52)
                {
                    var query = from c in context.TableBChx700
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 53)
                {
                    var query = from c in context.TableBChx700
                                where c.Id == 5
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }
          
                if (idbesoin == 54)
                {
                    var query = from c in context.TableBChx800
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 55)
                {
                    var query = from c in context.TableBChx800
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 56)
                { 
                    var query = from c in context.TableBChx800
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 57)
                {
                    var query = from c in context.TableBChx800
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 58)
                {
                    var query = from c in context.TableBChx800
                                where c.Id == 5
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }
                //saut

                if (idbesoin == 61)
                {
                    var query = from c in context.TableBPou200
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 62)
                {
                    var query = from c in context.TableBPou200
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 63)
                {
                    var query = from c in context.TableBPou200
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 64)
                {
                    var query = from c in context.TableBPou300
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 65)
                {
                    var query = from c in context.TableBPou300
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 66)
                {
                    var query = from c in context.TableBPou300
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 67)
                {
                    var query = from c in context.TableBPou400
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 68)
                {
                    var query = from c in context.TableBPou400
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 69)
                {
                    var query = from c in context.TableBPou400
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 70)
                {
                    var query = from c in context.TableBPou450
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 71)
                {
                    var query = from c in context.TableBPou450
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 72)
                {
                    var query = from c in context.TableBPou450
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 73)
                {
                    var query = from c in context.TableBPou500
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 74)
                {
                    var query = from c in context.TableBPou500
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 75)
                {
                    var query = from c in context.TableBPou500
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 76)
                {
                    var query = from c in context.TableBPou550
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 77)
                {
                    var query = from c in context.TableBPou550
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 78)
                {
                    var query = from c in context.TableBPou550
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 79)
                {
                    var query = from c in context.TableBPou600
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 80)
                {
                    var query = from c in context.TableBPou600
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 81)
                {
                    var query = from c in context.TableBPou600
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 82)
                {
                    var query = from c in context.TableBPou700
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 83)
                {
                    var query = from c in context.TableBPou700
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 84)
                {
                    var query = from c in context.TableBPou700
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 85)
                {
                    var query = from c in context.TableBPou800
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 86)
                {
                    var query = from c in context.TableBPou800
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 87)
                {
                    var query = from c in context.TableBPou800
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 88)
                {
                    var query = from c in context.TableBJum200
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 89)
                {
                    var query = from c in context.TableBJum200
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }
            
                if (idbesoin == 90)
                {
                    var query = from c in context.TableBJum200
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 91)
                {
                    var query = from c in context.TableBJum200
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 92)
                {
                    var query = from c in context.TableBJum200
                                where c.Id == 5
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 93)
                {
                    var query = from c in context.TableBJum200
                                where c.Id == 6
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 94)
                {
                    var query = from c in context.TableBJum200
                                where c.Id == 7
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 95)
                {
                    var query = from c in context.TableBJum200
                                where c.Id == 8
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 96)
                {
                    var query = from c in context.TableBJum200
                                where c.Id == 9
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 97)
                {
                    var query = from c in context.TableBJum200
                                where c.Id == 10
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 98)
                {
                    var query = from c in context.TableBJum200
                                where c.Id == 11
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 99)
                {
                    var query = from c in context.TableBJum200
                                where c.Id == 12
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 100)
                {
                    var query = from c in context.TableBJum200
                                where c.Id == 13
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 101)
                {
                    var query = from c in context.TableBJum200
                                where c.Id == 14
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 102)
                {
                    var query = from c in context.TableBJum300
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 103)
                {
                    var query = from c in context.TableBJum300
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 104)
                {
                    var query = from c in context.TableBJum300
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 105)
                {
                    var query = from c in context.TableBJum300
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 106)
                {
                    var query = from c in context.TableBJum300
                                where c.Id == 5
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 107)
                {
                    var query = from c in context.TableBJum300
                                where c.Id == 6
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 108)
                {
                    var query = from c in context.TableBJum300
                                where c.Id == 7
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 109)
                {
                    var query = from c in context.TableBJum300
                                where c.Id == 8
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 110)
                {
                    var query = from c in context.TableBJum300
                                where c.Id == 9
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 111)
                {
                    var query = from c in context.TableBJum300
                                where c.Id == 10
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 112)
                {
                    var query = from c in context.TableBJum300
                                where c.Id == 11
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 113)
                {
                    var query = from c in context.TableBJum300
                                where c.Id == 12
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 114)
                {
                    var query = from c in context.TableBJum300
                                where c.Id == 13
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 115)
                {
                    var query = from c in context.TableBJum300
                                where c.Id == 14
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 116)
                {
                    var query = from c in context.TableBJum400
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 117)
                {
                    var query = from c in context.TableBJum400
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 118)
                {
                    var query = from c in context.TableBJum400
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 119)
                {
                    var query = from c in context.TableBJum400
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 120)
                {
                    var query = from c in context.TableBJum400
                                where c.Id == 5
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 121)
                {
                    var query = from c in context.TableBJum400
                                where c.Id == 6
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 122)
                {
                    var query = from c in context.TableBJum400
                                where c.Id == 7
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 123)
                {
                    var query = from c in context.TableBJum400
                                where c.Id == 8
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 124)
                {
                    var query = from c in context.TableBJum400
                                where c.Id == 9
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 125)
                {
                    var query = from c in context.TableBJum400
                                where c.Id == 10
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 126)
                {
                    var query = from c in context.TableBJum400
                                where c.Id == 11
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 127)
                {
                    var query = from c in context.TableBJum400
                                where c.Id == 12
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 128)
                {
                    var query = from c in context.TableBJum400
                                where c.Id == 13
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 129)
                {
                    var query = from c in context.TableBJum400
                                where c.Id == 14
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 130)
                {
                    var query = from c in context.TableBJum450
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 131)
                {
                    var query = from c in context.TableBJum450
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 132)
                {
                    var query = from c in context.TableBJum450
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 133)
                {
                    var query = from c in context.TableBJum450
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 134)
                {
                    var query = from c in context.TableBJum450
                                where c.Id == 5
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 135)
                {
                    var query = from c in context.TableBJum450
                                where c.Id == 6
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 136)
                {
                    var query = from c in context.TableBJum450
                                where c.Id == 7
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 137)
                {
                    var query = from c in context.TableBJum450
                                where c.Id == 8
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 138)
                {
                    var query = from c in context.TableBJum450
                                where c.Id == 9
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 139)
                {
                    var query = from c in context.TableBJum450
                                where c.Id == 10
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 140)
                {
                    var query = from c in context.TableBJum450
                                where c.Id == 11
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 141)
                {
                    var query = from c in context.TableBJum450
                                where c.Id == 12
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 142)
                {
                    var query = from c in context.TableBJum450
                                where c.Id == 13
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 143)
                {
                    var query = from c in context.TableBJum450
                                where c.Id == 14
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 144)
                {
                    var query = from c in context.TableBJum500
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 145)
                {
                    var query = from c in context.TableBJum500
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 146)
                {
                    var query = from c in context.TableBJum500
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 147)
                {
                    var query = from c in context.TableBJum500
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 148)
                {
                    var query = from c in context.TableBJum500
                                where c.Id == 5
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 149)
                {
                    var query = from c in context.TableBJum500
                                where c.Id == 6
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 150)
                {
                    var query = from c in context.TableBJum500
                                where c.Id == 7
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 151)
                {
                    var query = from c in context.TableBJum500
                                where c.Id == 8
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 152)
                {
                    var query = from c in context.TableBJum500
                                where c.Id == 9
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 153)
                {
                    var query = from c in context.TableBJum500
                                where c.Id == 10
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 154)
                {
                    var query = from c in context.TableBJum500
                                where c.Id == 11
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 155)
                {
                    var query = from c in context.TableBJum500
                                where c.Id == 12
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 156)
                {
                    var query = from c in context.TableBJum500
                                where c.Id == 13
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 157)
                {
                    var query = from c in context.TableBJum500
                                where c.Id == 14
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 158)
                {
                    var query = from c in context.TableBJum550
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 159)
                {
                    var query = from c in context.TableBJum550
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 160)
                {
                    var query = from c in context.TableBJum550
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 161)
                {
                    var query = from c in context.TableBJum550
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 162)
                {
                    var query = from c in context.TableBJum550
                                where c.Id == 5
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 163)
                {
                    var query = from c in context.TableBJum550
                                where c.Id == 6
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 164)
                {
                    var query = from c in context.TableBJum550
                                where c.Id == 7
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 165)
                {
                    var query = from c in context.TableBJum550
                                where c.Id == 8
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 166)
                {
                    var query = from c in context.TableBJum550
                                where c.Id == 9
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 167)
                {
                    var query = from c in context.TableBJum550
                                where c.Id == 10
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 168)
                {
                    var query = from c in context.TableBJum550
                                where c.Id == 11
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 169)
                {
                    var query = from c in context.TableBJum550
                                where c.Id == 12
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 170)
                {
                    var query = from c in context.TableBJum550
                                where c.Id == 13
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 171)
                {
                    var query = from c in context.TableBJum550
                                where c.Id == 14
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 172)
                {
                    var query = from c in context.TableBJum600
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 173)
                {
                    var query = from c in context.TableBJum600
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 174)
                {
                    var query = from c in context.TableBJum600
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 175)
                {
                    var query = from c in context.TableBJum600
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 176)
                {
                    var query = from c in context.TableBJum600
                                where c.Id == 5
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 177)
                {
                    var query = from c in context.TableBJum600
                                where c.Id == 6
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 178)
                {
                    var query = from c in context.TableBJum600
                                where c.Id == 7
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 179)
                {
                    var query = from c in context.TableBJum600
                                where c.Id == 8
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 180)
                {
                    var query = from c in context.TableBJum600
                                where c.Id == 9
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 181)
                {
                    var query = from c in context.TableBJum600
                                where c.Id == 10
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 182)
                {
                    var query = from c in context.TableBJum600
                                where c.Id == 11
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 183)
                {
                    var query = from c in context.TableBJum600
                                where c.Id == 12
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 184)
                {
                    var query = from c in context.TableBJum600
                                where c.Id == 13
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 185)
                {
                    var query = from c in context.TableBJum600
                                where c.Id == 14
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 186)
                {
                    var query = from c in context.TableBJum700
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 187)
                {
                    var query = from c in context.TableBJum700
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 188)
                {
                    var query = from c in context.TableBJum700
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 189)
                {
                    var query = from c in context.TableBJum700
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 190)
                {
                    var query = from c in context.TableBJum700
                                where c.Id == 5
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 191)
                {
                    var query = from c in context.TableBJum700
                                where c.Id == 6
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 192)
                {
                    var query = from c in context.TableBJum700
                                where c.Id == 7
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 193)
                {
                    var query = from c in context.TableBJum700
                                where c.Id == 8
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 194)
                {
                    var query = from c in context.TableBJum700
                                where c.Id == 9
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 195)
                {
                    var query = from c in context.TableBJum700
                                where c.Id == 10
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 196)
                {
                    var query = from c in context.TableBJum700
                                where c.Id == 11
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 197)
                {
                    var query = from c in context.TableBJum700
                                where c.Id == 12
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 198)
                {
                    var query = from c in context.TableBJum700
                                where c.Id == 13
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 199)
                {
                    var query = from c in context.TableBJum700
                                where c.Id == 14
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 200)
                {
                    var query = from c in context.TableBJum800
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 201)
                {
                    var query = from c in context.TableBJum800
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 202)
                {
                    var query = from c in context.TableBJum800
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 203)
                {
                    var query = from c in context.TableBJum800
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 204)
                {
                    var query = from c in context.TableBJum800
                                where c.Id == 5
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 205)
                {
                    var query = from c in context.TableBJum800
                                where c.Id == 6
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 206)
                {
                    var query = from c in context.TableBJum800
                                where c.Id == 7
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 207)
                {
                    var query = from c in context.TableBJum800
                                where c.Id == 8
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 208)
                {
                    var query = from c in context.TableBJum800
                                where c.Id == 9
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 209)
                {
                    var query = from c in context.TableBJum800
                                where c.Id == 10
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 210)
                {
                    var query = from c in context.TableBJum800
                                where c.Id == 11
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 211)
                {
                    var query = from c in context.TableBJum800
                                where c.Id == 12
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 212)
                {
                    var query = from c in context.TableBJum800
                                where c.Id == 13
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 213)
                {
                    var query = from c in context.TableBJum800
                                where c.Id == 14
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 214)
                {
                    var query = from c in context.TableBEnt200
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 215)
                {
                    var query = from c in context.TableBEnt200
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 216)
                {
                    var query = from c in context.TableBEnt200
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 217)
                {
                    var query = from c in context.TableBEnt200
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 218)
                {
                    var query = from c in context.TableBEnt300
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 219)
                {
                    var query = from c in context.TableBEnt300
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 220)
                {
                    var query = from c in context.TableBEnt300
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 221)
                {
                    var query = from c in context.TableBEnt300
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 222)
                {
                    var query = from c in context.TableBEnt400
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 223)
                {
                    var query = from c in context.TableBEnt400
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 224)
                {
                    var query = from c in context.TableBEnt400
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 225)
                {
                    var query = from c in context.TableBEnt400
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 226)
                {
                    var query = from c in context.TableBEnt450
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 227)
                {
                    var query = from c in context.TableBEnt450
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 228)
                {
                    var query = from c in context.TableBEnt450
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 229)
                {
                    var query = from c in context.TableBEnt450
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 230)
                {
                    var query = from c in context.TableBEnt500
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 231)
                {
                    var query = from c in context.TableBEnt500
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 232)
                {
                    var query = from c in context.TableBEnt500
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 233)
                {
                    var query = from c in context.TableBEnt500
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 234)
                {
                    var query = from c in context.TableBEnt550
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 235)
                {
                    var query = from c in context.TableBEnt550
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 236)
                {
                    var query = from c in context.TableBEnt550
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 237)
                {
                    var query = from c in context.TableBEnt550
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 238)
                {
                    var query = from c in context.TableBEnt600
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 239)
                {
                    var query = from c in context.TableBEnt600
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 240)
                {
                    var query = from c in context.TableBEnt600
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 241)
                {
                    var query = from c in context.TableBEnt600
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 242)
                {
                    var query = from c in context.TableBEnt700
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 243)
                {
                    var query = from c in context.TableBEnt700
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 244)
                {
                    var query = from c in context.TableBEnt700
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 245)
                {
                    var query = from c in context.TableBEnt700
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 246)
                {
                    var query = from c in context.TableBEnt800
                                where c.Id == 1
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 247)
                {
                    var query = from c in context.TableBEnt800
                                where c.Id == 2
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 248)
                {
                    var query = from c in context.TableBEnt800
                                where c.Id == 3
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

                if (idbesoin == 249)
                {
                    var query = from c in context.TableBEnt800
                                where c.Id == 4
                                select new { c.MS__Kg_, c.UFC, c.MADC__g_, c.Ca__g_, c.P__g_, c.Zn__mg_, c.Cu__mg_ };
                    var results = query.ToList();
                    datagridBesoins.ItemsSource = results;

                    foreach (var i in query)
                    {
                        besoin.MS__Kg_ = i.MS__Kg_;

                        besoin.UFC = i.UFC;

                        besoin.MADC__g_ = i.MADC__g_;

                        besoin.Ca__g_ = i.Ca__g_;

                        besoin.P__g_ = i.P__g_;

                        besoin.Zn__mg_ = i.Zn__mg_;

                        besoin.Cu__mg_ = i.Cu__mg_;
                    }

                }

            }
        }



        List<int> rowCount = new List<int> { 1 };                                                                                           // Compteur de nouvelles lignes
        List<int> delCount = new List<int> { };                                                                                             // Compteur de lignes supprimées

        private void btnAdd_Click(object sender, RoutedEventArgs e)                                                                         // Ajoute une ligne (la derniere supprimée)
        {

            if (rowCount.Count < 8)
            {
                if (delCount.Count > 0)
                {
                    gridapports.RowDefinitions[delCount.Last()].Height = new GridLength(1, GridUnitType.Star);
                    rowCount.Add(delCount.Last());
                    delCount.Remove(delCount.Last());



                }
                else
                {
                    gridapports.RowDefinitions[rowCount.Count + 1].Height = new GridLength(1, GridUnitType.Star);
                    rowCount.Add(rowCount.Count + 1);
                }
            }
            else
                btnAdd.Visibility = Visibility.Collapsed;


        }

        private void btnDel2_Click(object sender, RoutedEventArgs e)                                                                        // Supprime la ligne actuelle et son objet
        {
            gridapports.RowDefinitions[2].Height = new GridLength(0);
            rowCount.Remove(2);
            delCount.Add(2);
            cbaliment2.SelectedIndex = 0;

            textboxQ2.Text = string.Empty;

            btnAdd.Visibility = Visibility.Visible;
        }

        private void btnDel3_Click(object sender, RoutedEventArgs e)
        {
            gridapports.RowDefinitions[3].Height = new GridLength(0);
            rowCount.Remove(3);
            delCount.Add(3);
            cbaliment3.SelectedIndex = 0;

            textboxQ3.Text = string.Empty;

            btnAdd.Visibility = Visibility.Visible;
        }

        private void btnDel4_Click(object sender, RoutedEventArgs e)
        {
            gridapports.RowDefinitions[4].Height = new GridLength(0);
            rowCount.Remove(4);
            delCount.Add(4);
            cbaliment4.SelectedIndex = 0;

            textboxQ4.Text = string.Empty;

            btnAdd.Visibility = Visibility.Visible;
        }

        private void btnDel5_Click(object sender, RoutedEventArgs e)
        {
            gridapports.RowDefinitions[5].Height = new GridLength(0);
            rowCount.Remove(5);
            delCount.Add(5);
            cbaliment5.SelectedIndex = 0;

            textboxQ5.Text = string.Empty;

            btnAdd.Visibility = Visibility.Visible;
        }

        private void btnDel6_Click(object sender, RoutedEventArgs e)
        {
            gridapports.RowDefinitions[6].Height = new GridLength(0);
            rowCount.Remove(6);
            delCount.Add(6);
            cbaliment6.SelectedIndex = 0;

            textboxQ6.Text = string.Empty;

            btnAdd.Visibility = Visibility.Visible;
        }

        private void btnDel7_Click(object sender, RoutedEventArgs e)
        {
            gridapports.RowDefinitions[7].Height = new GridLength(0);
            rowCount.Remove(7);
            delCount.Add(7);
            cbaliment7.SelectedIndex = 0;

            textboxQ7.Text = string.Empty;

            btnAdd.Visibility = Visibility.Visible;
        }

        private void btnDel8_Click(object sender, RoutedEventArgs e)
        {
            gridapports.RowDefinitions[8].Height = new GridLength(0);
            rowCount.Remove(8);
            delCount.Add(8);
            cbaliment8.SelectedIndex = 0;

            textboxQ8.Text = string.Empty;

            btnAdd.Visibility = Visibility.Visible;
        }



        Apport Apport1 = new Apport();                                                                                                      // Crée une liste d'objets sérialisable

        Donnees apport1 = new Donnees();                                                                                                    // Crée les ogjets Donnees et leurs listes

        DonneesP apportP1 = new DonneesP();

        List<DonneesP> apportPlist1 = new List<DonneesP>();

        private void cbaliment1_SelectionChanged(object sender, SelectionChangedEventArgs e)                                                // Evenement liste aliment changée
        {

            string q;
            q = textboxQ1.Text;
            decimal qNum;
            decimal.TryParse(q, out qNum);

            datagrid1.ItemsSource = null;

            using (var context = new BddChevalimEntities())
            {
                if (listBoxCategorie.SelectedIndex == -1)
                {
                    apportP1.MS__Kg_ = 0;

                    apportP1.UFC = 0;

                    apportP1.MADC__g_ = 0;

                    apportP1.Ca__g_ = 0;

                    apportP1.P__g_ = 0;

                    apportP1.Zn__mg_ = 0;

                    apportP1.Cu__mg_ = 0;


                    apportPlist1.Clear();

                    datagrid1bis.ItemsSource = null;

                }

      
                    var query = from c in context.TableAliments
                                where c.Id == cbaliment1.SelectedIndex + 1
                                select new { c.MS, c.UFC, c.MADC_g, c.Ca_g, c.P_g, c.Zn_mg, c.Cu_mg};
                    var results = query.ToList();
                    datagrid1.ItemsSource = results;

                    Apport1.cbindex = cbaliment1.SelectedIndex;

                    apport1.Q = qNum;

                    foreach (var i in query)
                    {
                    apport1.ms = i.MS;

                    apport1.ufc = i.UFC;

                    apport1.madc = i.MADC_g;

                    apport1.ca = i.Ca_g;
                
                    apport1.p = i.P_g;

                    apport1.zn = i.Zn_mg;

                    apport1.cu = i.Cu_mg;



                    }

                    apportP1.MS__Kg_ = apport1.ms * apport1.Q;

                    apportP1.UFC = apport1.ufc * apport1.Q;

                    apportP1.MADC__g_ = apport1.madc * apport1.Q;

                    apportP1.Ca__g_ = apport1.ca * apport1.Q;

                    apportP1.P__g_ = apport1.p * apport1.Q;

                    apportP1.Zn__mg_ = apport1.zn * apport1.Q;

                    apportP1.Cu__mg_ = apport1.cu * apport1.Q;


                    apportPlist1.Clear();
                    apportPlist1.Add(apportP1);

                    datagrid1bis.ItemsSource = null;
                    datagrid1bis.ItemsSource = apportPlist1;
                }
             
            

            Somme();
            Difference();

            PrixRation();
        }


        Apport Apport2 = new Apport();

        Donnees apport2 = new Donnees();

        DonneesP apportP2 = new DonneesP();

        List<DonneesP> apportPlist2 = new List<DonneesP>();

        private void cbaliment2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string q;
            q = textboxQ2.Text;
            decimal qNum;
            decimal.TryParse(q, out qNum);

            datagrid2.ItemsSource = null;

            using (var context = new BddChevalimEntities())
            {

                if (listBoxCategorie.SelectedIndex == -1)
                {
                    apportP2.MS__Kg_ = 0;

                    apportP2.UFC = 0;

                    apportP2.MADC__g_ = 0;

                    apportP2.Ca__g_ = 0;

                    apportP2.P__g_ = 0;

                    apportP2.Zn__mg_ = 0;

                    apportP2.Cu__mg_ = 0;

                    apportPlist2.Clear();

                    datagrid2bis.ItemsSource = null;
                }

        
                    var query = from c in context.TableAliments
                                where c.Id == cbaliment2.SelectedIndex + 1
                                select new { c.MS, c.UFC, c.MADC_g, c.Ca_g, c.P_g, c.Zn_mg, c.Cu_mg};
                    var results = query.ToList();
                    datagrid2.ItemsSource = results;

                Apport2.cbindex = cbaliment2.SelectedIndex;

                apport2.Q = qNum;

                    foreach (var i in query)
                    {
                    apport2.ms = i.MS;

                    apport2.ufc = i.UFC;

                    apport2.madc = i.MADC_g;

                    apport2.ca = i.Ca_g;
                
                    apport2.p = i.P_g;

                    apport2.zn = i.Zn_mg;

                    apport2.cu = i.Cu_mg;

                

                    }

                    apportP2.MS__Kg_ = apport2.ms * apport2.Q;

                    apportP2.UFC = apport2.ufc * apport2.Q;

                    apportP2.MADC__g_ = apport2.madc * apport2.Q;

                    apportP2.Ca__g_ = apport2.ca * apport2.Q;

                    apportP2.P__g_ = apport2.p * apport2.Q;

                    apportP2.Zn__mg_ = apport2.zn * apport2.Q;

                    apportP2.Cu__mg_ = apport2.cu * apport2.Q;


                    apportPlist2.Clear();
                    apportPlist2.Add(apportP2);

                    datagrid2bis.ItemsSource = null;
                    datagrid2bis.ItemsSource = apportPlist2;
                }
             
            

            Somme();
            Difference();

            PrixRation();
        }


        Apport Apport3 = new Apport();

        Donnees apport3 = new Donnees();

        DonneesP apportP3 = new DonneesP();

        List<DonneesP> apportPlist3 = new List<DonneesP>();

        private void cbaliment3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string q;
            q = textboxQ3.Text;
            decimal qNum;
            decimal.TryParse(q, out qNum);

            datagrid3.ItemsSource = null;

            using (var context = new BddChevalimEntities())
            {

                if (listBoxCategorie.SelectedIndex == -1)
                {
                    apportP3.MS__Kg_ = 0;

                    apportP3.UFC = 0;

                    apportP3.MADC__g_ = 0;

                    apportP3.Ca__g_ = 0;

                    apportP3.P__g_ = 0;

                    apportP3.Zn__mg_ = 0;

                    apportP3.Cu__mg_ = 0;

                    apportPlist3.Clear();

                    datagrid3bis.ItemsSource = null;

                }

            
                    var query = from c in context.TableAliments
                                where c.Id == cbaliment3.SelectedIndex + 1
                                select new { c.MS, c.UFC, c.MADC_g, c.Ca_g, c.P_g, c.Zn_mg, c.Cu_mg};
                    var results = query.ToList();
                    datagrid3.ItemsSource = results;

                Apport3.cbindex = cbaliment3.SelectedIndex;

                apport3.Q = qNum;

                    foreach (var i in query)
                    {
                    apport3.ms = i.MS;

                    apport3.ufc = i.UFC;

                    apport3.madc = i.MADC_g;

                    apport3.ca = i.Ca_g;

                    apport3.p = i.P_g;

                    apport3.zn = i.Zn_mg;

                    apport3.cu = i.Cu_mg;

                

                    }

                    apportP3.MS__Kg_ = apport3.ms * apport3.Q;

                    apportP3.UFC = apport3.ufc * apport3.Q;

                    apportP3.MADC__g_ = apport3.madc * apport3.Q;

                    apportP3.Ca__g_ = apport3.ca * apport3.Q;

                    apportP3.P__g_ = apport3.p * apport3.Q;

                    apportP3.Zn__mg_ = apport3.zn * apport3.Q;

                    apportP3.Cu__mg_ = apport3.cu * apport3.Q;


                    apportPlist3.Clear();
                    apportPlist3.Add(apportP3);

                    datagrid3bis.ItemsSource = null;
                    datagrid3bis.ItemsSource = apportPlist3;
                }
            
            

            Somme();
            Difference();

            PrixRation();
        }


        Apport Apport4 = new Apport();

        Donnees apport4 = new Donnees();

        DonneesP apportP4 = new DonneesP();

        List<DonneesP> apportPlist4 = new List<DonneesP>();

        private void cbaliment4_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string q;
            q = textboxQ4.Text;
            decimal qNum;
            decimal.TryParse(q, out qNum);

            datagrid4.ItemsSource = null;

            using (var context = new BddChevalimEntities())
            {
                if (listBoxCategorie.SelectedIndex == -1)
                {
                    apportP4.MS__Kg_ = 0;

                    apportP4.UFC = 0;

                    apportP4.MADC__g_ = 0;

                    apportP4.Ca__g_ = 0;

                    apportP4.P__g_ = 0;

                    apportP4.Zn__mg_ = 0;

                    apportP4.Cu__mg_ = 0;


                    apportPlist4.Clear();

                    datagrid4bis.ItemsSource = null;

                }

           
                    var query = from c in context.TableAliments
                                where c.Id == cbaliment4.SelectedIndex + 1
                                select new { c.MS, c.UFC, c.MADC_g, c.Ca_g, c.P_g, c.Zn_mg, c.Cu_mg};
                    var results = query.ToList();
                    datagrid4.ItemsSource = results;

                Apport4.cbindex = cbaliment4.SelectedIndex;

                apport4.Q = qNum;

                    foreach (var i in query)
                    {
                    apport4.ms = i.MS;

                    apport4.ufc = i.UFC;

                    apport4.madc = i.MADC_g;

                    apport4.ca = i.Ca_g;

                    apport4.p = i.P_g;

                    apport4.zn = i.Zn_mg;

                    apport4.cu = i.Cu_mg;



                }

                    apportP4.MS__Kg_ = apport4.ms * apport4.Q;

                    apportP4.UFC = apport4.ufc * apport4.Q;

                    apportP4.MADC__g_ = apport4.madc * apport4.Q;

                    apportP4.Ca__g_ = apport4.ca * apport4.Q;

                    apportP4.P__g_ = apport4.p * apport4.Q;

                    apportP4.Zn__mg_ = apport4.zn * apport4.Q;

                    apportP4.Cu__mg_ = apport4.cu * apport4.Q;


                    apportPlist4.Clear();
                    apportPlist4.Add(apportP4);

                    datagrid4bis.ItemsSource = null;
                    datagrid4bis.ItemsSource = apportPlist4;
                }
          
            

            Somme();
            Difference();

            PrixRation();
        }


        Apport Apport5 = new Apport();

        Donnees apport5 = new Donnees();

        DonneesP apportP5 = new DonneesP();

        List<DonneesP> apportPlist5 = new List<DonneesP>();

        private void cbaliment5_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string q;
            q = textboxQ5.Text;
            decimal qNum;
            decimal.TryParse(q, out qNum);

            datagrid5.ItemsSource = null;

            using (var context = new BddChevalimEntities())
            {
                if (listBoxCategorie.SelectedIndex == -1)
                {
                    apportP5.MS__Kg_ = 0;

                    apportP5.UFC = 0;

                    apportP5.MADC__g_ = 0;

                    apportP5.Ca__g_ = 0;

                    apportP5.P__g_ = 0;

                    apportP5.Zn__mg_ = 0;

                    apportP5.Cu__mg_ = 0;


                    apportPlist5.Clear();

                    datagrid5bis.ItemsSource = null;

                }


                    var query = from c in context.TableAliments
                                where c.Id == cbaliment5.SelectedIndex + 1
                                select new { c.MS, c.UFC, c.MADC_g, c.Ca_g, c.P_g, c.Zn_mg, c.Cu_mg};
                    var results = query.ToList();
                    datagrid5.ItemsSource = results;

                Apport5.cbindex = cbaliment5.SelectedIndex;

                apport5.Q = qNum;

                    foreach (var i in query)
                    {
                    apport5.ms = i.MS;

                    apport5.ufc = i.UFC;

                    apport5.madc = i.MADC_g;

                    apport5.ca = i.Ca_g;

                    apport5.p = i.P_g;

                    apport5.zn = i.Zn_mg;

                    apport5.cu = i.Cu_mg;

              

                }

                    apportP5.MS__Kg_ = apport5.ms * apport5.Q;

                    apportP5.UFC = apport5.ufc * apport5.Q;

                    apportP5.MADC__g_ = apport5.madc * apport5.Q;

                    apportP5.Ca__g_ = apport5.ca * apport5.Q;

                    apportP5.P__g_ = apport5.p * apport5.Q;

                    apportP5.Zn__mg_ = apport5.zn * apport5.Q;

                    apportP5.Cu__mg_ = apport5.cu * apport5.Q;


                    apportPlist5.Clear();
                    apportPlist5.Add(apportP5);

                    datagrid5bis.ItemsSource = null;
                    datagrid5bis.ItemsSource = apportPlist5;
                }
             
            

            Somme();
            Difference();

            PrixRation();
        }


        Apport Apport6 = new Apport();

        Donnees apport6 = new Donnees();

        DonneesP apportP6 = new DonneesP();

        List<DonneesP> apportPlist6 = new List<DonneesP>();

        private void cbaliment6_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string q;
            q = textboxQ6.Text;
            decimal qNum;
            decimal.TryParse(q, out qNum);

            datagrid6.ItemsSource = null;

            using (var context = new BddChevalimEntities())
            {

                if (listBoxCategorie.SelectedIndex == -1)
                {

                    apportP6.MS__Kg_ = 0;

                    apportP6.UFC = 0;

                    apportP6.MADC__g_ = 0;

                    apportP6.Ca__g_ = 0;

                    apportP6.P__g_ = 0;

                    apportP6.Zn__mg_ = 0;

                    apportP6.Cu__mg_ = 0;


                    apportPlist6.Clear();

                    datagrid6bis.ItemsSource = null;

                }


                    var query = from c in context.TableAliments
                                where c.Id == cbaliment6.SelectedIndex + 1
                                select new { c.MS, c.UFC, c.MADC_g, c.Ca_g, c.P_g, c.Zn_mg, c.Cu_mg};
                    var results = query.ToList();
                    datagrid6.ItemsSource = results;

                Apport6.cbindex = cbaliment6.SelectedIndex;

                apport6.Q = qNum;

                    foreach (var i in query)
                    {
                    apport6.ms = i.MS;

                    apport6.ufc = i.UFC;

                    apport6.madc = i.MADC_g;

                    apport6.ca = i.Ca_g;

                    apport6.p = i.P_g;

                    apport6.zn = i.Zn_mg;

                    apport6.cu = i.Cu_mg;

          

                }

                    apportP6.MS__Kg_ = apport6.ms * apport6.Q;

                    apportP6.UFC = apport6.ufc * apport6.Q;

                    apportP6.MADC__g_ = apport6.madc * apport6.Q;

                    apportP6.Ca__g_ = apport6.ca * apport6.Q;

                    apportP6.P__g_ = apport6.p * apport6.Q;

                    apportP6.Zn__mg_ = apport6.zn * apport6.Q;

                    apportP6.Cu__mg_ = apport6.cu * apport6.Q;


                    apportPlist6.Clear();
                    apportPlist6.Add(apportP6);

                    datagrid6bis.ItemsSource = null;
                    datagrid6bis.ItemsSource = apportPlist6;
                }
         
            

            Somme();
            Difference();

            PrixRation();
        }


        Apport Apport7 = new Apport();

        Donnees apport7 = new Donnees();

        DonneesP apportP7 = new DonneesP();

        List<DonneesP> apportPlist7 = new List<DonneesP>();

        private void cbaliment7_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string q;
            q = textboxQ7.Text;
            decimal qNum;
            decimal.TryParse(q, out qNum);

            datagrid7.ItemsSource = null;

            using (var context = new BddChevalimEntities())
            {

                if (listBoxCategorie.SelectedIndex == -1)
                {
                    apportP7.MS__Kg_ = 0;

                    apportP7.UFC = 0;

                    apportP7.MADC__g_ = 0;

                    apportP7.Ca__g_ = 0;

                    apportP7.P__g_ = 0;

                    apportP7.Zn__mg_ = 0;

                    apportP7.Cu__mg_ = 0;


                    apportPlist7.Clear();

                    datagrid7bis.ItemsSource = null;

                }

              
                    var query = from c in context.TableAliments
                                where c.Id == cbaliment7.SelectedIndex + 1
                                select new { c.MS, c.UFC, c.MADC_g, c.Ca_g, c.P_g, c.Zn_mg, c.Cu_mg};
                    var results = query.ToList();
                    datagrid7.ItemsSource = results;

                Apport7.cbindex = cbaliment7.SelectedIndex;

                apport7.Q = qNum;

                    foreach (var i in query)
                    {
                    apport7.ms = i.MS;

                    apport7.ufc = i.UFC;

                    apport7.madc = i.MADC_g;

                    apport7.ca = i.Ca_g;

                    apport7.p = i.P_g;

                    apport7.zn = i.Zn_mg;

                    apport7.cu = i.Cu_mg;

           

                }

                    apportP7.MS__Kg_ = apport7.ms * apport7.Q;

                    apportP7.UFC = apport7.ufc * apport7.Q;

                    apportP7.MADC__g_ = apport7.madc * apport7.Q;

                    apportP7.Ca__g_ = apport7.ca * apport7.Q;

                    apportP7.P__g_ = apport7.p * apport7.Q;

                    apportP7.Zn__mg_ = apport7.zn * apport7.Q;

                    apportP7.Cu__mg_ = apport7.cu * apport7.Q;


                    apportPlist7.Clear();
                    apportPlist7.Add(apportP7);

                    datagrid7bis.ItemsSource = null;
                    datagrid7bis.ItemsSource = apportPlist7;
                }
             
            Somme();
            Difference();

            PrixRation();
        }


        Apport Apport8 = new Apport();

        Donnees apport8 = new Donnees();

        DonneesP apportP8 = new DonneesP();

        List<DonneesP> apportPlist8 = new List<DonneesP>();

        private void cbaliment8_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string q;
            q = textboxQ8.Text;
            decimal qNum;
            decimal.TryParse(q, out qNum);

            datagrid8.ItemsSource = null;

            using (var context = new BddChevalimEntities())
            {

                if (listBoxCategorie.SelectedIndex == -1)
                {
                    apportP8.MS__Kg_ = 0;

                    apportP8.UFC = 0;

                    apportP8.MADC__g_ = 0;

                    apportP8.Ca__g_ = 0;

                    apportP8.P__g_ = 0;

                    apportP8.Zn__mg_ = 0;

                    apportP8.Cu__mg_ = 0;


                    apportPlist8.Clear();

                    datagrid8bis.ItemsSource = null;

                }


                var query = from c in context.TableAliments
                            where c.Id == cbaliment8.SelectedIndex + 1
                            select new { c.MS, c.UFC, c.MADC_g, c.Ca_g, c.P_g, c.Zn_mg, c.Cu_mg};
                var results = query.ToList();
                datagrid8.ItemsSource = results;

                Apport8.cbindex = cbaliment8.SelectedIndex;

                apport8.Q = qNum;

                foreach (var i in query)
                {
                    apport8.ms = i.MS;

                    apport8.ufc = i.UFC;

                    apport8.madc = i.MADC_g;

                    apport8.ca = i.Ca_g;

                    apport8.p = i.P_g;

                    apport8.zn = i.Zn_mg;

                    apport8.cu = i.Cu_mg;

                 

                }

                apportP8.MS__Kg_ = apport8.ms * apport8.Q;

                apportP8.UFC = apport8.ufc * apport8.Q;

                apportP8.MADC__g_ = apport8.madc * apport8.Q;

                apportP8.Ca__g_ = apport8.ca * apport8.Q;

                apportP8.P__g_ = apport8.p * apport8.Q;

                apportP8.Zn__mg_ = apport8.zn * apport8.Q;

                apportP8.Cu__mg_ = apport8.cu * apport8.Q;


                apportPlist8.Clear();
                apportPlist8.Add(apportP8);

                datagrid8bis.ItemsSource = null;
                datagrid8bis.ItemsSource = apportPlist8;


            }

            Somme();
            Difference();

            PrixRation();
        }
        


        private void infopoidsvol_Click(object sender, RoutedEventArgs e)                                                                    // btn info poids / volumes
        {
            MessageBoxResult infovolumes = MessageBox.Show("- Avoine entière : 0,5 Kg/l \r\n\r\n- Avoine aplatie : 0,22 Kg/l \r\n\r\n- Orge entière : 0,6 Kg/l \r\n\r\n- Orge aplatie : 0,3 à 0,4 Kg/l \r\n\r\n- Orge floconnée : 0,45 Kg/l \r\n\r\n- Granulé de luzerne : 0,6 Kg/l \r\n\r\n- Mais concassé : 0,6 Kg/l \r\n\r\n- Tourteau de soja : 0,6 Kg/l \r\n\r\n- Granulés industriels : 0,5 à 0,6 Kg/l \r\n\r\n- Floconnés industriels : 0,35 à 0,5 Kg/l", "Poids (Kg) / Volume (l) des aliments usuels");
        }



        private void textboxQ1_TextChanged(object sender, TextChangedEventArgs e)                                                            // Evenement Quantité modifiée         
        {
            string q;
            q = textboxQ1.Text;
            decimal qNum;
            decimal.TryParse(q, out qNum);

                apport1.Q = qNum;
                Apport1.Q = qNum;

                apportP1.MS__Kg_ = apport1.ms * apport1.Q;

                apportP1.UFC = apport1.ufc * apport1.Q;

                apportP1.MADC__g_ = apport1.madc * apport1.Q;

                apportP1.Ca__g_ = apport1.ca * apport1.Q;

                apportP1.P__g_ = apport1.p * apport1.Q;

                apportP1.Zn__mg_ = apport1.zn * apport1.Q;

                apportP1.Cu__mg_ = apport1.cu * apport1.Q;


                apportPlist1.Clear();
                apportPlist1.Add(apportP1);

                datagrid1bis.ItemsSource = null;
                datagrid1bis.ItemsSource = apportPlist1;


            Somme();
            Difference();

            PrixRation();
        }

        private void textboxQ2_TextChanged(object sender, TextChangedEventArgs e)
        {
            string q;
            q = textboxQ2.Text;
            decimal qNum;
            decimal.TryParse(q, out qNum);

                apport2.Q = qNum;
                Apport2.Q = qNum;

                apportP2.MS__Kg_ = apport2.ms * apport2.Q;

                apportP2.UFC = apport2.ufc * apport2.Q;

                apportP2.MADC__g_ = apport2.madc * apport2.Q;

                apportP2.Ca__g_ = apport2.ca * apport2.Q;

                apportP2.P__g_ = apport2.p * apport2.Q;

                apportP2.Zn__mg_ = apport2.zn * apport2.Q;

                apportP2.Cu__mg_ = apport2.cu * apport2.Q;


                apportPlist2.Clear();
                apportPlist2.Add(apportP2);

                datagrid2bis.ItemsSource = null;
                datagrid2bis.ItemsSource = apportPlist2;


            Somme();
            Difference();

            PrixRation();
        }

        private void textboxQ3_TextChanged(object sender, TextChangedEventArgs e)
        {
            string q;
            q = textboxQ3.Text;
            decimal qNum;
            decimal.TryParse(q, out qNum);

        
                apport3.Q = qNum;
                Apport3.Q = qNum;

                apportP3.MS__Kg_ = apport3.ms * apport3.Q;

                apportP3.UFC = apport3.ufc * apport3.Q;

                apportP3.MADC__g_ = apport3.madc * apport3.Q;

                apportP3.Ca__g_ = apport3.ca * apport3.Q;

                apportP3.P__g_ = apport3.p * apport3.Q;

                apportP3.Zn__mg_ = apport3.zn * apport3.Q;

                apportP3.Cu__mg_ = apport3.cu * apport3.Q;


                apportPlist3.Clear();
                apportPlist3.Add(apportP3);

                datagrid3bis.ItemsSource = null;
                datagrid3bis.ItemsSource = apportPlist3;


            Somme();
            Difference();

            PrixRation();
        }

        private void textboxQ4_TextChanged(object sender, TextChangedEventArgs e)
        {
            string q;
            q = textboxQ4.Text;
            decimal qNum;
            decimal.TryParse(q, out qNum);

                apport4.Q = qNum;
                Apport4.Q = qNum;

                apportP4.MS__Kg_ = apport4.ms * apport4.Q;

                apportP4.UFC = apport4.ufc * apport4.Q;

                apportP4.MADC__g_ = apport4.madc * apport4.Q;

                apportP4.Ca__g_ = apport4.ca * apport4.Q;

                apportP4.P__g_ = apport4.p * apport4.Q;

                apportP4.Zn__mg_ = apport4.zn * apport4.Q;

                apportP4.Cu__mg_ = apport4.cu * apport4.Q;


                apportPlist4.Clear();
                apportPlist4.Add(apportP4);

                datagrid4bis.ItemsSource = null;
                datagrid4bis.ItemsSource = apportPlist4;


            Somme();
            Difference();

            PrixRation();
        }

        private void textboxQ5_TextChanged(object sender, TextChangedEventArgs e)
        {
            string q;
            q = textboxQ5.Text;
            decimal qNum;
            decimal.TryParse(q, out qNum);

       
                apport5.Q = qNum;
                Apport5.Q = qNum;

                apportP5.MS__Kg_ = apport5.ms * apport5.Q;

                apportP5.UFC = apport5.ufc * apport5.Q;

                apportP5.MADC__g_ = apport5.madc * apport5.Q;

                apportP5.Ca__g_ = apport5.ca * apport5.Q;

                apportP5.P__g_ = apport5.p * apport5.Q;

                apportP5.Zn__mg_ = apport5.zn * apport5.Q;

                apportP5.Cu__mg_ = apport5.cu * apport5.Q;


                apportPlist5.Clear();
                apportPlist5.Add(apportP5);

                datagrid5bis.ItemsSource = null;
                datagrid5bis.ItemsSource = apportPlist5;

            Somme();
            Difference();

            PrixRation();
        }

        private void textboxQ6_TextChanged(object sender, TextChangedEventArgs e)
        {
            string q;
            q = textboxQ6.Text;
            decimal qNum;
            decimal.TryParse(q, out qNum);

      
                apport6.Q = qNum;
                Apport6.Q = qNum;

                apportP6.MS__Kg_ = apport6.ms * apport6.Q;

                apportP6.UFC = apport6.ufc * apport6.Q;

                apportP6.MADC__g_ = apport6.madc * apport6.Q;

                apportP6.Ca__g_ = apport6.ca * apport6.Q;

                apportP6.P__g_ = apport6.p * apport6.Q;

                apportP6.Zn__mg_ = apport6.zn * apport6.Q;

                apportP6.Cu__mg_ = apport6.cu * apport6.Q;


                apportPlist6.Clear();
                apportPlist6.Add(apportP6);

                datagrid6bis.ItemsSource = null;
                datagrid6bis.ItemsSource = apportPlist6;
            
            Somme();
            Difference();

            PrixRation();
        }

        private void textboxQ7_TextChanged(object sender, TextChangedEventArgs e)
        {
            string q;
            q = textboxQ7.Text;
            decimal qNum;
            decimal.TryParse(q, out qNum);

    
                apport7.Q = qNum;
                Apport7.Q = qNum;

                apportP7.MS__Kg_ = apport7.ms * apport7.Q;

                apportP7.UFC = apport7.ufc * apport7.Q;

                apportP7.MADC__g_ = apport7.madc * apport7.Q;

                apportP7.Ca__g_ = apport7.ca * apport7.Q;

                apportP7.P__g_ = apport7.p * apport7.Q;

                apportP7.Zn__mg_ = apport7.zn * apport7.Q;

                apportP7.Cu__mg_ = apport7.cu * apport7.Q;


                apportPlist7.Clear();
                apportPlist7.Add(apportP7);

                datagrid7bis.ItemsSource = null;
                datagrid7bis.ItemsSource = apportPlist7;


            Somme();
            Difference();

            PrixRation();
        }

        private void textboxQ8_TextChanged(object sender, TextChangedEventArgs e)
        {
            string q;
            decimal qNum;
            q = textboxQ8.Text;
            decimal.TryParse(q, out qNum);

      
                apport8.Q = qNum;
                Apport8.Q = qNum;

                apportP8.MS__Kg_ = apport8.ms * apport8.Q;

                apportP8.UFC = apport8.ufc * apport8.Q;

                apportP8.MADC__g_ = apport8.madc * apport8.Q;

                apportP8.Ca__g_ = apport8.ca * apport8.Q;

                apportP8.P__g_ = apport8.p * apport8.Q;

                apportP8.Zn__mg_ = apport8.zn * apport8.Q;

                apportP8.Cu__mg_ = apport8.cu * apport8.Q;


                apportPlist8.Clear();
                apportPlist8.Add(apportP8);

                datagrid8bis.ItemsSource = null;
                datagrid8bis.ItemsSource = apportPlist8;

            Somme();
            Difference();

            PrixRation();
        }



        DonneesP somme = new DonneesP();
        List<DonneesP> sommelist = new List<DonneesP>();

        private void Somme()                                                                                                                 // Methode Somme apports
        {

            sommelist.Clear();
            datagridsomme.ItemsSource = null;





                somme.MS__Kg_ = apportP1.MS__Kg_ + apportP2.MS__Kg_ + apportP3.MS__Kg_ + apportP4.MS__Kg_ + apportP5.MS__Kg_ + apportP6.MS__Kg_ + apportP7.MS__Kg_ + apportP8.MS__Kg_;

                somme.UFC = apportP1.UFC + apportP2.UFC + apportP3.UFC + apportP4.UFC + apportP5.UFC + apportP6.UFC + apportP7.UFC + apportP8.UFC;

                somme.MADC__g_ = apportP1.MADC__g_ + apportP2.MADC__g_ + apportP3.MADC__g_ + apportP4.MADC__g_ + apportP5.MADC__g_ + apportP6.MADC__g_ + apportP7.MADC__g_ + apportP8.MADC__g_;

                somme.Ca__g_ = apportP1.Ca__g_ + apportP2.Ca__g_ + apportP3.Ca__g_ + apportP4.Ca__g_ + apportP5.Ca__g_ + apportP6.Ca__g_ + apportP7.Ca__g_ + apportP8.Ca__g_;

                somme.P__g_ = apportP1.P__g_ + apportP2.P__g_ + apportP3.P__g_ + apportP4.P__g_ + apportP5.P__g_ + apportP6.P__g_ + apportP7.P__g_ + apportP8.P__g_;

                somme.Zn__mg_ = apportP1.Zn__mg_ + apportP2.Zn__mg_ + apportP3.Zn__mg_ + apportP4.Zn__mg_ + apportP5.Zn__mg_ + apportP6.Zn__mg_ + apportP7.Zn__mg_ + apportP8.Zn__mg_;

                somme.Cu__mg_ = apportP1.Cu__mg_ + apportP2.Cu__mg_ + apportP3.Cu__mg_ + apportP4.Cu__mg_ + apportP5.Cu__mg_ + apportP6.Cu__mg_ + apportP7.Cu__mg_ + apportP8.Cu__mg_;



                sommelist.Add(somme);


                datagridsomme.ItemsSource = sommelist;

            

            

            CalculRapports();
            
        }


        DonneesP difference = new DonneesP();
        List<DonneesP> differencelist = new List<DonneesP>();

        private void Difference()                                                                                                           // Methode difference besoins - somme apport
        {

            differencelist.Clear();

            datagriddiff.ItemsSource = null;
            
            

                difference.MS__Kg_ = somme.MS__Kg_ - besoin.MS__Kg_;

                difference.UFC = somme.UFC - besoin.UFC;

                difference.MADC__g_ = somme.MADC__g_ - besoin.MADC__g_;

                difference.Ca__g_ = somme.Ca__g_ - besoin.Ca__g_;

                difference.P__g_ = somme.P__g_ - besoin.P__g_;

                difference.Zn__mg_ = somme.Zn__mg_ - besoin.Zn__mg_;

                difference.Cu__mg_ = somme.Cu__mg_ - besoin.Cu__mg_;



                differencelist.Add(difference);


                datagriddiff.ItemsSource = differencelist;

        }


        private void infoprecision_Click(object sender, RoutedEventArgs e)                                                                  // Btn info Précision
        {
            MessageBoxResult infoprécision = MessageBox.Show("Le pourcentage indiqué représente les seuils (+ et -) de coloration des cellules de la ligne \"Ecarts (A-B)\". \n\r\n\rCes seuils sont calculés par rapport aux besoins soit : \n\r\n\rSeuils = +ou- (Seuils de coloration %) x Besoin \n\r\n\rAinsi, une valeur à 20% autorise des apports exédentaires ou insuffisants de 20% des besoins. \n\r\n\rOn considère par exemple, dans ce cas précis, que 12 Kg de Matière Sèche pour un besoin de 10 Kg conviennent mais pas plus, ou la cellule se colorera en rouge. \n\rDe la même manière, on ne tolèrera pas moins de 8 Kg de MS, au delà, la celulle se colorera en bleu. ", "Seuils de coloration des cellules");
        }

        decimal facteur;
        private void tbprecision_TextChanged(object sender, TextChangedEventArgs e)                                                         // Modif precision
        {
            string Precision;
            Precision = tbprecision.Text;
            decimal precision;
            decimal.TryParse(Precision, out precision);
            facteur = precision / 100;

            if(listBoxCategorie.SelectedIndex != -1)
            Difference();
        }

        private void datagriddiff_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)                              // Couleurs cellules datagriddiff
        {

            if (e.PropertyName == "MS__Kg_")
            {
                    if (difference.MS__Kg_ < -(facteur * besoin.MS__Kg_))
                    {
                        e.Column.CellStyle = new Style(typeof(DataGridCell));
                        e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.CornflowerBlue)));
                    }
                    if (difference.MS__Kg_ >= -(facteur * besoin.MS__Kg_) && difference.MS__Kg_ <= (facteur * besoin.MS__Kg_))
                    {
                        e.Column.CellStyle = new Style(typeof(DataGridCell));
                        e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.YellowGreen)));
                    }
                    if (difference.MS__Kg_ > (facteur * besoin.MS__Kg_))
                    {
                        e.Column.CellStyle = new Style(typeof(DataGridCell));
                        e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.IndianRed)));
                    }
            }

            if (e.PropertyName == "UFC")
            {
                    if (difference.UFC < -(facteur * besoin.UFC))
                    {
                        e.Column.CellStyle = new Style(typeof(DataGridCell));
                        e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.CornflowerBlue)));
                    }
                    if (difference.UFC >= -(facteur * besoin.UFC) && difference.UFC <= (facteur * besoin.UFC))
                    {
                        e.Column.CellStyle = new Style(typeof(DataGridCell));
                        e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.YellowGreen)));
                    }
                    if (difference.UFC > (facteur * besoin.UFC))
                    {
                        e.Column.CellStyle = new Style(typeof(DataGridCell));
                        e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.IndianRed)));
                    }
            }

           if (e.PropertyName == "MADC__g_")
           {
                   if (difference.MADC__g_ < -(facteur * besoin.MADC__g_))
                   {
                       e.Column.CellStyle = new Style(typeof(DataGridCell));
                       e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.CornflowerBlue)));
                   }
                   if (difference.MADC__g_ >= -(facteur * besoin.MADC__g_) && difference.MADC__g_ <= (facteur * besoin.MADC__g_))
                   {
                       e.Column.CellStyle = new Style(typeof(DataGridCell));
                       e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.YellowGreen)));
                   }
                   if (difference.MADC__g_ > (facteur * besoin.MADC__g_))
                   {
                        e.Column.CellStyle = new Style(typeof(DataGridCell));
                        e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.IndianRed)));
                   }
           }

           if (e.PropertyName == "Ca__g_")
           {
                   if (difference.Ca__g_ < -(facteur * besoin.Ca__g_))
                   {
                        e.Column.CellStyle = new Style(typeof(DataGridCell));
                        e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.CornflowerBlue)));
                   }
                   if (difference.Ca__g_ >= -(facteur * besoin.Ca__g_) && difference.Ca__g_ <= (facteur * besoin.Ca__g_))
                   {
                        e.Column.CellStyle = new Style(typeof(DataGridCell));
                        e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.YellowGreen)));
                   }
                   if (difference.Ca__g_ > (facteur * besoin.Ca__g_))
                   {
                        e.Column.CellStyle = new Style(typeof(DataGridCell));
                        e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.IndianRed)));
                   }

           }
           if (e.PropertyName == "P__g_")
           {
                    if (difference.P__g_ < -(facteur * besoin.P__g_))
                    {
                        e.Column.CellStyle = new Style(typeof(DataGridCell));
                        e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.CornflowerBlue)));
                    }
                    if (difference.P__g_ >= -(facteur * besoin.P__g_) && difference.P__g_ <= (facteur * besoin.P__g_))
                    {
                        e.Column.CellStyle = new Style(typeof(DataGridCell));
                        e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.YellowGreen)));
                    }
                    if (difference.P__g_ > (facteur * besoin.P__g_))
                    {
                        e.Column.CellStyle = new Style(typeof(DataGridCell));
                        e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.IndianRed)));
                    }

           }
           if (e.PropertyName == "Zn__mg_")
           {
                    if (difference.Zn__mg_ < -(facteur * besoin.Zn__mg_))
                    {
                        e.Column.CellStyle = new Style(typeof(DataGridCell));
                        e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.CornflowerBlue)));
                    }
                    if (difference.Zn__mg_ >= -(facteur * besoin.Zn__mg_) && difference.Zn__mg_ <= (facteur * besoin.Zn__mg_))
                    {
                        e.Column.CellStyle = new Style(typeof(DataGridCell));
                        e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.YellowGreen)));
                    }
                    if (difference.Zn__mg_ > (facteur * besoin.Zn__mg_))
                    {
                        e.Column.CellStyle = new Style(typeof(DataGridCell));
                        e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.IndianRed)));
                    }

           }
           if (e.PropertyName == "Cu__mg_")
           {
                    if (difference.Cu__mg_ < -(facteur * besoin.Cu__mg_))
                    {
                        e.Column.CellStyle = new Style(typeof(DataGridCell));
                        e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.CornflowerBlue)));
                    }
                    if (difference.Cu__mg_ >= -(facteur * besoin.Cu__mg_) && difference.Cu__mg_ <= (facteur * besoin.Cu__mg_))
                    {
                        e.Column.CellStyle = new Style(typeof(DataGridCell));
                        e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.YellowGreen)));
                    }
                    if (difference.Cu__mg_ > (facteur * besoin.Cu__mg_))
                    {
                        e.Column.CellStyle = new Style(typeof(DataGridCell));
                        e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.IndianRed)));
                    }
           }
            
        }



        private void CalculRapports()                                                                                                       // Calculs rapports
        {

                tbmsufc.Text = string.Empty;
                tbmadcufc.Text = string.Empty;
                tbcap.Text = string.Empty;
                tbzncu.Text = string.Empty;
            

                if (somme.UFC != null)
                {
                    double msufc = (double)somme.MS__Kg_ / (double)somme.UFC;
                    tbmsufc.Text = Math.Round(msufc, 2).ToString();

                    double madcufc = (double)somme.MADC__g_ / (double)somme.UFC;
                    tbmadcufc.Text = Math.Round(madcufc, 2).ToString();
                }
                if (somme.P__g_ != null)
                {
                    double cap = (double)somme.Ca__g_ / (double)somme.P__g_;
                    tbcap.Text = Math.Round(cap, 2).ToString();
                }
                if (somme.Cu__mg_ != null)
                {
                    double zncu = (double)somme.Zn__mg_ / (double)somme.Cu__mg_;
                    tbzncu.Text = Math.Round(zncu, 2).ToString();
                }
        }

        private void tbmsufc_TextChanged(object sender, TextChangedEventArgs e)                                                             // Couleurs textbox rapports
        {
            tbmsufc.Background = Brushes.White;

            string s;
            s = tbmsufc.Text;
            double d;
            double.TryParse(s, out d);

            if (d < 1.2 && d > 0)
            {
                tbmsufc.Background = Brushes.CornflowerBlue;
            }

            if (d >= 1.2 && d <= 1.9)
            {
                tbmsufc.Background = Brushes.YellowGreen;
            }

            if (d > 1.9)
            {
                tbmsufc.Background = Brushes.IndianRed;
            }

        }

        private void tbmadcufc_TextChanged(object sender, TextChangedEventArgs e)
        {
            tbmadcufc.Background = Brushes.White;

            string s;
            s = tbmadcufc.Text;
            double d;
            double.TryParse(s, out d);

            if (d < 70 && d > 0)
            {
                tbmadcufc.Background = Brushes.CornflowerBlue;
            }

            if (d >= 70 && d <= 120)
            {
                tbmadcufc.Background = Brushes.YellowGreen;
            }

            if (d > 120)
            {
                tbmadcufc.Background = Brushes.IndianRed;
            }
          
        }

        private void tbcap_TextChanged(object sender, TextChangedEventArgs e)
        {
            tbcap.Background = Brushes.White;

            string s;
            s = tbcap.Text;
            double d;
            double.TryParse(s, out d);

            if (d < 1.5 && d > 0)
            {
                tbcap.Background = Brushes.CornflowerBlue;
            }

            if (d >= 1.5 && d <= 2)
            {
                tbcap.Background = Brushes.YellowGreen;
            }

            if (d > 2)
            {
                tbcap.Background = Brushes.IndianRed;
            }

        }

        private void tbzncu_TextChanged(object sender, TextChangedEventArgs e)
        {
            tbzncu.Background = Brushes.White;  

            string s;
            s = tbzncu.Text;
            double d;
            double.TryParse(s, out d);

            if (d < 3 && d > 0)
            {
                tbzncu.Background = Brushes.CornflowerBlue;
            }

            if (d >= 3 && d <= 4.1)
            {
                tbzncu.Background = Brushes.YellowGreen;
            }

            if (d > 4.1)
            {
                tbzncu.Background = Brushes.IndianRed;
            }
            
        }


        private void PrixRation()                                                                                                           // Calcul prix ration
        {

            if (cbaliment1.SelectedIndex > 0)
            {
                label1.Content = "Aliment 1 :";

                using (BddChevalimEntities context = new BddChevalimEntities())
                {
                    var query = context.TableAliments.Where(c => c.Id == cbaliment1.SelectedIndex + 1).First();

                    cbalim1.Visibility = Visibility.Visible;
                    cbalim1.SelectedIndex = cbaliment1.SelectedIndex;
                    apport1.prix = query.Prix_Kg;
                    tbprixKg1.Text = apport1.prix.ToString() + " Euros/Kg";
                    tbQ1.Text = "x " + apport1.Q.ToString() + " Kg";
                    tbprix1.Text = "= " + (apport1.prix * apport1.Q).ToString() + " Euros/Jour";
                }
            }
            else
            {
                label1.Content = string.Empty;
                cbalim1.Visibility = Visibility.Hidden;
                tbprixKg1.Text = string.Empty;
                tbQ1.Text = string.Empty;
                tbprix1.Text = string.Empty;
            }

            if (cbaliment2.SelectedIndex > 0)
            {
                label2.Content = "Aliment 2 :";

                using (BddChevalimEntities context = new BddChevalimEntities())
                {
                    var query = context.TableAliments.Where(c => c.Id == cbaliment2.SelectedIndex + 1).First();


                    cbalim2.Visibility = Visibility.Visible;
                    cbalim2.SelectedIndex = cbaliment2.SelectedIndex;
                    apport2.prix = query.Prix_Kg;
                    tbprixKg2.Text = apport2.prix.ToString() + " Euros/Kg";
                    tbQ2.Text = "x " + apport2.Q.ToString() + " Kg";
                    tbprix2.Text = "= " + (apport2.prix * apport2.Q).ToString() + " Euros/Jour";
                }
            }
            else
            {
                label2.Content = string.Empty;
                cbalim2.Visibility = Visibility.Hidden;
                tbprixKg2.Text = string.Empty;
                tbQ2.Text = string.Empty;
                tbprix2.Text = string.Empty;
            }

            if (cbaliment3.SelectedIndex > 0)
            {
                label3.Content = "Aliment 3 :";

                using (BddChevalimEntities context = new BddChevalimEntities())
                {
                    var query = context.TableAliments.Where(c => c.Id == cbaliment3.SelectedIndex + 1).First();


                    cbalim3.Visibility = Visibility.Visible;
                    cbalim3.SelectedIndex = cbaliment3.SelectedIndex;
                    apport3.prix = query.Prix_Kg;
                    tbprixKg3.Text = apport3.prix.ToString() + " Euros/Kg";
                    tbQ3.Text = "x " + apport3.Q.ToString() + " Kg";
                    tbprix3.Text = "= " + (apport3.prix * apport3.Q).ToString() + " Euros/Jour";
                }
            }
            else
            {
                label3.Content = string.Empty;
                cbalim3.Visibility = Visibility.Hidden;
                tbprixKg3.Text = string.Empty;
                tbQ3.Text = string.Empty;
                tbprix3.Text = string.Empty;
            }

            if (cbaliment4.SelectedIndex > 0)
            {
                label4.Content = "Aliment 4 :";

                using (BddChevalimEntities context = new BddChevalimEntities())
                {
                    var query = context.TableAliments.Where(c => c.Id == cbaliment4.SelectedIndex + 1).First();


                    cbalim4.Visibility = Visibility.Visible;
                    cbalim4.SelectedIndex = cbaliment4.SelectedIndex;
                    apport4.prix = query.Prix_Kg;
                    tbprixKg4.Text = apport4.prix.ToString() + " Euros/Kg";
                    tbQ4.Text = "x " + apport4.Q.ToString() + " Kg";
                    tbprix4.Text = "= " + (apport4.prix * apport4.Q).ToString() + " Euros/Jour";
                }
            }
            else
            {
                label4.Content = string.Empty;
                cbalim4.Visibility = Visibility.Hidden;
                tbprixKg4.Text = string.Empty;
                tbQ4.Text = string.Empty;
                tbprix4.Text = string.Empty;
            }

            if (cbaliment5.SelectedIndex > 0)
            {
                label5.Content = "Aliment 5 :";

                using (BddChevalimEntities context = new BddChevalimEntities())
                {
                    var query = context.TableAliments.Where(c => c.Id == cbaliment5.SelectedIndex + 1).First();


                    cbalim5.Visibility = Visibility.Visible;
                    cbalim5.SelectedIndex = cbaliment5.SelectedIndex;
                    apport5.prix = query.Prix_Kg;
                    tbprixKg5.Text = apport5.prix.ToString() + " Euros/Kg";
                    tbQ5.Text = "x " + apport5.Q.ToString() + " Kg";
                    tbprix5.Text = "= " + (apport5.prix * apport5.Q).ToString() + " Euros/Jour";
                }
            }
            else
            {
                label5.Content = string.Empty;
                cbalim5.Visibility = Visibility.Hidden;
                tbprixKg5.Text = string.Empty;
                tbQ5.Text = string.Empty;
                tbprix5.Text = string.Empty;
            }

            if (cbaliment6.SelectedIndex > 0)
            {
                label6.Content = "Aliment 6 :";

                using (BddChevalimEntities context = new BddChevalimEntities())
                {
                    var query = context.TableAliments.Where(c => c.Id == cbaliment6.SelectedIndex + 1).First();


                    cbalim6.Visibility = Visibility.Visible;
                    cbalim6.SelectedIndex = cbaliment6.SelectedIndex;
                    apport6.prix = query.Prix_Kg;
                    tbprixKg6.Text = apport6.prix.ToString() + " Euros/Kg";
                    tbQ6.Text = "x " + apport6.Q.ToString() + " Kg";
                    tbprix6.Text = "= " + (apport6.prix * apport6.Q).ToString() + " Euros/Jour";
                }
            }
            else
            {
                label6.Content = string.Empty;
                cbalim6.Visibility = Visibility.Hidden;
                tbprixKg6.Text = string.Empty;
                tbQ6.Text = string.Empty;
                tbprix6.Text = string.Empty;
            }

            if (cbaliment7.SelectedIndex > 0)
            {
                label7.Content = "Aliment 7 :";

                using (BddChevalimEntities context = new BddChevalimEntities())
                {
                    var query = context.TableAliments.Where(c => c.Id == cbaliment7.SelectedIndex + 1).First();


                    cbalim7.Visibility = Visibility.Visible;
                    cbalim7.SelectedIndex = cbaliment7.SelectedIndex;
                    apport7.prix = query.Prix_Kg;
                    tbprixKg7.Text = apport7.prix.ToString() + " Euros/Kg";
                    tbQ7.Text = "x " + apport7.Q.ToString() + " Kg";
                    tbprix7.Text = "= " + (apport7.prix * apport7.Q).ToString() + " Euros/Jour";
                }
            }
            else
            {
                label7.Content = string.Empty;
                cbalim7.Visibility = Visibility.Hidden;
                tbprixKg7.Text = string.Empty;
                tbQ7.Text = string.Empty;
                tbprix7.Text = string.Empty;
            }

            if (cbaliment8.SelectedIndex > 0)
            {
                label8.Content = "Aliment 8 :";

                using (BddChevalimEntities context = new BddChevalimEntities())
                {
                    var query = context.TableAliments.Where(c => c.Id == cbaliment8.SelectedIndex + 1).First();


                    cbalim8.Visibility = Visibility.Visible;
                    cbalim8.SelectedIndex = cbaliment8.SelectedIndex;
                    apport8.prix = query.Prix_Kg;
                    tbprixKg8.Text = apport8.prix.ToString() + " Euros/Kg";
                    tbQ8.Text = "x " + apport8.Q.ToString() + " Kg";
                    tbprix8.Text = "= " + (apport8.prix * apport8.Q).ToString() + " Euros/Jour";
                }
            }
            else
            {
                label8.Content = string.Empty;
                cbalim8.Visibility = Visibility.Hidden;
                tbprixKg8.Text = string.Empty;
                tbQ8.Text = string.Empty;
                tbprix8.Text = string.Empty;
            }

            tbQTotal.Text = (apport1.Q + apport2.Q + apport3.Q + apport4.Q + apport5.Q + apport6.Q + apport6.Q + apport8.Q).ToString() + " Kg";

            tbprixTotal.Text = ((apport1.prix * apport1.Q) + (apport2.prix * apport2.Q) + (apport3.prix * apport3.Q) + (apport4.prix * apport4.Q) + (apport5.prix * apport5.Q) + (apport6.prix * apport6.Q) + (apport7.prix * apport7.Q) + (apport8.prix * apport8.Q)).ToString() + " Euros/jour";

            tbprixTotalAn.Text = (365 * ((apport1.prix * apport1.Q) + (apport2.prix * apport2.Q) + (apport3.prix * apport3.Q) + (apport4.prix * apport4.Q) + (apport5.prix * apport5.Q) + (apport6.prix * apport6.Q) + (apport7.prix * apport7.Q) + (apport8.prix * apport8.Q))).ToString() + " Euros/an";
        }


        private void listBoxECategorie_SelectionChanged(object sender, SelectionChangedEventArgs e)                                         // Evenement "selection changée" de la liste Catégorie de l'Estimateur de Poids
        {
            if (listBoxECategorie.SelectedIndex == 1)
                textBoxH.IsEnabled = false;
            else textBoxH.IsEnabled = true;

            Estimateur();
        }

        private void Estimateur()                                                                                                           // Estimateur de Poids
        {
            string H = textBoxH.Text;
            int HNum;

            string T = textBoxT.Text;
            int TNum;

            switch (listBoxECategorie.SelectedIndex)
            {
                default:

                    textBlockR.Text = "Veuillez selectionner une catégorie ...";

                    break;

                case 0:

                    int.TryParse(T, out TNum);
                    int.TryParse(H, out HNum);

                    if (TNum == 0 | HNum == 0)
                    {
                        textBlockR.Text = "Veuillez entrer vos valeurs (nombres entiers)";
                    }
                    else
                    {
                        double Result0 = (4.3 * TNum + 3 * HNum - 785);

                        if (Result0 <= 0)
                        {
                            textBlockR.Text = "Ooops, une des valeurs est trop faible ...";
                        }
                        else
                        {
                            textBlockR.Text = "Votre Jument / Cheval pèse environ : " + Result0 + " Kg (+/- 26 Kg)";
                        }
                    }

                    break;

                case 1:

                    int.TryParse(T, out TNum);

                    if (TNum == 0)
                    {
                        textBlockR.Text = "Veuillez entrer une valeur pour T (nombres entiers)";
                    }
                    else
                    {
                        double Result2 = (4.5 * TNum - 370);
                        if (Result2 <= 0)
                        {
                            textBlockR.Text = "Ooops, une des valeurs est trop faible ...";
                        }
                        else
                        {
                            textBlockR.Text = "Votre Poulain pèse environ : " + Result2 + " Kg (+/- 23 Kg)";
                        }
                    }

                    break;

                case 2:

                    int.TryParse(T, out TNum);
                    int.TryParse(H, out HNum);

                    if (TNum == 0 | HNum == 0)
                    {
                        textBlockR.Text = "Veuillez entrer vos valeurs (nombres entiers)";
                    }
                    else
                    {
                        double Result1 = (5.2 * TNum + 2.6 * HNum - 855);
                        if (Result1 <= 0)
                        {
                            textBlockR.Text = "Ooops, une des valeurs est trop faible ...";
                        }
                        else
                        {
                            textBlockR.Text = "Votre Jument pèse environ : " + Result1 + " Kg (+/- 25 Kg)";
                        }

                    }
                    break;

                case 3:

                    int.TryParse(T, out TNum);
                    int.TryParse(H, out HNum);

                    if (TNum == 0 | HNum == 0)
                    {
                        textBlockR.Text = "Veuillez entrer vos valeurs (nombres entiers)";
                    }
                    else
                    {
                        double Result0 = (7.3 * TNum + 3 * HNum - 800);

                        if (Result0 <= 0)
                        {
                            textBlockR.Text = "Ooops, une des valeurs est trop faible ...";
                        }
                        else
                        {
                            textBlockR.Text = "Votre Entier pèse environ : " + Result0 + " Kg (+/- 27 Kg)";
                        }
                    }

                    break;
            }
        }

        private void textBoxT_TextChanged(object sender, TextChangedEventArgs e)
        {
            Estimateur();
        }

        private void textBoxH_TextChanged(object sender, TextChangedEventArgs e)
        {
            Estimateur();
        }



        private void infoajoutalim_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Veuillez renseigner chaque case avec les valeurs appropriées et valider la ligne en cliquant sur le boutton \"Ajouter\". \r\n\r\nVotre aliment apparaîtra alors dans la table des aliments et sera sélectionnable depuis la feuille de calcul.\r\n\r\nVous pourrez modifier ses valeurs à tout moment depuis l'outil : \"Modifier un aliment\"", "Ajouter un Aliment");
        }

        private void btnajoutalim_Click(object sender, RoutedEventArgs e)                                                                   // Ajouter un aliment à la base
        {
            if (tbaliment.Text != string.Empty)
            {
                if (MessageBox.Show("Voulez vous vraiment ajouter cet Aliment ? \r\n\r\n(Procédure irréversible)", "Confirmation Ajout", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {

                    using (var context = new BddChevalimEntities())
                    {
                        var newalim = context.TableAliments.Create();

                        newalim.Id = tableAlimentsDataGrid.Items.Count + 1;

                        if (cbIcones.SelectedValue != null)
                        {
                            newalim.ImagePath = (cbIcones.SelectedItem as ComboBoxItem).Tag.ToString();
                        }

                        newalim.Aliments = tbaliment.Text;

                        decimal msNum;
                        decimal.TryParse(tbms.Text, out msNum);
                        newalim.MS = msNum;

                        decimal ufcNum;
                        decimal.TryParse(tbufc.Text, out ufcNum);
                        newalim.UFC = ufcNum;

                        decimal madcNum;
                        decimal.TryParse(tbmadc.Text, out madcNum);
                        newalim.MADC_g = madcNum;

                        decimal caNum;
                        decimal.TryParse(tbca.Text, out caNum);
                        newalim.Ca_g = caNum;

                        decimal pNum;
                        decimal.TryParse(tbp.Text, out pNum);
                        newalim.P_g = pNum;

                        decimal znNum;
                        decimal.TryParse(tbzn.Text, out znNum);
                        newalim.Zn_mg = znNum;

                        decimal cuNum;
                        decimal.TryParse(tbcu.Text, out cuNum);
                        newalim.Cu_mg = cuNum;

                        decimal prixNum;
                        decimal.TryParse(tbprix.Text, out prixNum);
                        newalim.Prix_Kg = prixNum;

                        if ( msNum == 0 || ufcNum == 0 || madcNum == 0 || caNum == 0 || pNum == 0 || znNum == 0 || cuNum == 0 || prixNum == 0)
                        {
                            MessageBox.Show("Au moins une des valeurs est nulle ou incorrecte \r\n\r\nVeuillez vous assurer que vos valeurs soient correctes (nombres décimaux, virgules \",\" uniquement, points \".\" interdits).", "Attention");
                        }

                        context.TableAliments.Add(newalim);

                        context.SaveChanges();

                        LoadRessources();

                        cbIcones.SelectedItem = null;
                        tbaliment.Text = string.Empty;
                        tbms.Text = string.Empty;
                        tbufc.Text = string.Empty;
                        tbmadc.Text = string.Empty;
                        tbca.Text = string.Empty;
                        tbp.Text = string.Empty;
                        tbzn.Text = string.Empty;
                        tbcu.Text = string.Empty;
                        tbprix.Text = string.Empty;
                    }

                }
            }

            else
            {
                MessageBox.Show("Vous n'avez pas donné de nom à votre aliment. \r\n\r\nVeuillez renseigner un nom dans la case \"Aliment\"", "Attention");
            }
        }

          

        private void infomodifalim_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Vous pouvez modifier un aliment à tout moment. \r\n\r\nSélectionnez simplement l'aliment à modifier dans la liste et éditez les valeurs souhaitées. \r\n\r\nLes valeurs se mettront automatiquement à jour dans la Table de Aliments, ainsi que dans la feuille de calul.", "Modifier un Aliment");
        }

        private void cbalimtoedit_SelectionChanged(object sender, SelectionChangedEventArgs e)                                              // Sélection aliment à modifier
        {
            if (cbalimtoedit.SelectedItem != null)
            {

                using (var context = new BddChevalimEntities())
                {
                    var alimtoedit = context.TableAliments.Where(c => c.Id == cbalimtoedit.SelectedIndex + 1).FirstOrDefault();

                    tbmstoedit.Text = alimtoedit.MS.ToString();

                    tbufctoedit.Text = alimtoedit.UFC.ToString();

                    tbmadctoedit.Text = alimtoedit.MADC_g.ToString();

                    tbcatoedit.Text = alimtoedit.Ca_g.ToString();

                    tbptoedit.Text = alimtoedit.P_g.ToString();

                    tbzntoedit.Text = alimtoedit.Zn_mg.ToString();

                    tbcutoedit.Text = alimtoedit.Cu_mg.ToString();

                    tbprixtoedit.Text = alimtoedit.Prix_Kg.ToString();
                }
            }
        }

        bool modified = false;

        private void btnmodifalim_Click(object sender, RoutedEventArgs e)                                                                   // Modifier un aliment
        {
            if (MessageBox.Show("Voulez vous vraiment modifier cet Aliment ?", "Confirmation Modification", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {

                TableAliments alimnew = new TableAliments();

                string mstoedit = tbmstoedit.Text;
                decimal mstoeditNum;
                decimal.TryParse(mstoedit, out mstoeditNum);
                alimnew.MS = mstoeditNum;

                string ufctoedit = tbufctoedit.Text;
                decimal ufctoeditNum;
                decimal.TryParse(ufctoedit, out ufctoeditNum);
                alimnew.UFC = ufctoeditNum;

                string madctoedit = tbmadctoedit.Text;
                decimal madctoeditNum;
                decimal.TryParse(madctoedit, out madctoeditNum);
                alimnew.MADC_g = madctoeditNum;

                string catoedit = tbcatoedit.Text;
                decimal catoeditNum;
                decimal.TryParse(catoedit, out catoeditNum);
                alimnew.Ca_g = catoeditNum;

                string ptoedit = tbptoedit.Text;
                decimal ptoeditNum;
                decimal.TryParse(ptoedit, out ptoeditNum);
                alimnew.P_g = ptoeditNum;

                string zntoedit = tbzntoedit.Text;
                decimal zntoeditNum;
                decimal.TryParse(zntoedit, out zntoeditNum);
                alimnew.Zn_mg = zntoeditNum;

                string cutoedit = tbcutoedit.Text;
                decimal cutoeditNum;
                decimal.TryParse(cutoedit, out cutoeditNum);
                alimnew.Cu_mg = cutoeditNum;

                string prixtoedit = tbprixtoedit.Text;
                decimal prixtoeditNum;
                decimal.TryParse(prixtoedit, out prixtoeditNum);
                alimnew.Prix_Kg = prixtoeditNum;


                if (mstoeditNum == 0 || ufctoeditNum == 0 || madctoeditNum == 0 || catoeditNum == 0 || ptoeditNum == 0 || zntoeditNum == 0 || cutoeditNum == 0 || prixtoeditNum == 0)
                {
                    MessageBox.Show("Au moins une des valeurs est nulle ou incorrecte \r\n\r\nVeuillez vous assurer que vos valeurs soient correctes (nombres décimaux, virgules \",\" uniquement, points \".\" interdits).", "Attention");
                }

                using (var context = new BddChevalimEntities())
                {

                    var alimtoedit = context.TableAliments.Where(c => c.Id == cbalimtoedit.SelectedIndex + 1).First();

                    alimtoedit.MS = alimnew.MS;

                    alimtoedit.UFC = alimnew.UFC;

                    alimtoedit.MADC_g = alimnew.MADC_g;

                    alimtoedit.Ca_g = alimnew.Ca_g;

                    alimtoedit.P_g = alimnew.P_g;

                    alimtoedit.Zn_mg = alimnew.Zn_mg;

                    alimtoedit.Cu_mg = alimnew.Cu_mg;

                    alimtoedit.Prix_Kg = alimnew.Prix_Kg;


                    context.SaveChanges();

                    modified = true;

                    LoadRessources();


                    cbalimtoedit.SelectedItem = null;

                    tbmstoedit.Text = string.Empty;
                    tbufctoedit.Text = string.Empty;
                    tbmadctoedit.Text = string.Empty;
                    tbcatoedit.Text = string.Empty;
                    tbptoedit.Text = string.Empty;
                    tbzntoedit.Text = string.Empty;
                    tbcutoedit.Text = string.Empty;
                    tbprixtoedit.Text = string.Empty;
                }

                
            }
        }



      
    }

}