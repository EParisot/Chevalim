﻿namespace Chevalim
{
    public class DonneesP
    {
        public decimal? MS__Kg_ { get; set; }
        public decimal? UFC { get; set; }
        public decimal? MADC__g_ { get; set; }
        public decimal? Ca__g_ { get; set; }
        public decimal? P__g_ { get; set; }
        public decimal? Zn__mg_ { get; set; }
        public decimal? Cu__mg_ { get; set; }

    }
}
