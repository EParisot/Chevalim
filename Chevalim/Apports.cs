﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chevalim
{
    public class Apports
    {
        private decimal? ms;
        public decimal? MS { get; set; }
        public decimal? UFC { get; set; }
        public decimal? MADC { get; set; }
        public decimal? Ca { get; set; }
        public decimal? P { get; set; }
        public decimal? Zn { get; set; }
        public decimal? Cu { get; set; }
    }
}
