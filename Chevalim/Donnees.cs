﻿using System.Collections.Generic;


namespace Chevalim
{


    public class Donnees : List<decimal?>
    {

        public decimal? cbindex { get; set; }

        public decimal? ms { get; set; }
        public decimal? ufc { get; set; }
        public decimal? madc { get; set; }
        public decimal? ca { get; set; }
        public decimal? p { get; set; }
        public decimal? zn { get; set; }
        public decimal? cu { get; set; }
        public decimal? prix { get; set; } = 0;
        public decimal Q { get; set; } = 0;


        


    }
}
