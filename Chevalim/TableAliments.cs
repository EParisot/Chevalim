//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Chevalim
{
    using System;
    using System.Collections.Generic;
    
    public partial class TableAliments
    {
        public int Id { get; set; }
        public string ImagePath { get; set; }
        public string Aliments { get; set; }
        public Nullable<decimal> MS { get; set; }
        public Nullable<decimal> UFC { get; set; }
        public Nullable<decimal> MADC_g { get; set; }
        public Nullable<decimal> Ca_g { get; set; }
        public Nullable<decimal> P_g { get; set; }
        public Nullable<decimal> Zn_mg { get; set; }
        public Nullable<decimal> Cu_mg { get; set; }
        public Nullable<decimal> Prix_Kg { get; set; }
    }
}
